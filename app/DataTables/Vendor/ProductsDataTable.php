<?php

namespace App\DataTables\Vendor;


use App\Models\Products;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {


        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {
                $profile_url = route('products.show',$row->id);
                $delete_url =  route('products.destroy',$row->id);
                return '<a href=' . $profile_url . '  class="btn btn-outline-primary">View</a> &nbsp; &nbsp; <form method="POST" action="'.$delete_url.'">
                                                         '.method_field('DELETE').'
                                                          '.csrf_field().'
                                                          <button type="submit" class="btn btn-outline-danger" title="Delete">Delete</button>
                                                        </form>';
                         })
            ->editColumn('image_url', function ($row) {
                return '<span style="width: 120px; max-height: 120px;"><img src=' . $row->image_url . ' border="0" width="100%" style="width: 120px; max-height: 120px;" class="img-rounded" align="center" /></span>';
            })
            ->editColumn('status', function ($row) {
                if ($row->status) {
                    return '<span style="color: #1b55e2;
                    border: 2px dashed #1b55e2; background: transparent;" class="shadow-none badge badge-primary">Active</span>';
                } else {
                    return '<span style="color: #1b55e2;
                                    border: 2px dashed #1b55e2; background: transparent;" class="shadow-none badge badge-danger">In-Active</span>';
                }
            })
            ->rawColumns(['image_url', 'status', 'action'])
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Vendor/ProductsDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductsDataTable $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('vendor/productsdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('id'),
            Column::make('add your columns'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Vendor/Products_' . date('YmdHis');
    }
}
