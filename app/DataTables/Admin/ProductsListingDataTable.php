<?php

namespace App\DataTables\Admin;


use App\Models\Products;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductsListingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()->eloquent($query)
                 /*      ->addColumn('action', function ($row) {
                           //$profile_url = route('customersList', ['id' => $row->id]);
                           $profile_url = 'productView/'.$row->products_id;
                           return '<a href=' . $profile_url. '  class="btn btn-primary">View</a>
                            <button type="button" id="'.$row->products_id.'" class="btn btn-danger delete">Delete</button>';
                       })*/
                    ->editColumn('image_url', function ($row) {
                          return '<img src='.asset($row->image_url).' border="0" width="100%" class="img-rounded" align="center" />';
                      })
                       ->editColumn('products_status', function ($row) {
                           if ($row->products_status) {
                               return '<span style="color: #1b55e2;
                                        border: 2px dashed #1b55e2; background: transparent;" class="shadow-none badge badge-primary">Approved</span>';
                           } else {
                               return '<span style="color: #1b55e2;
                                                        border: 2px dashed #1b55e2; background: transparent;" class="shadow-none badge badge-danger">Suspended</span>';
                           }
                       })

                       ->rawColumns(['status','image_url','products_status'])
                       ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin/ProductsListingDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Products $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('admin/productslistingdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('id'),
            Column::make('add your columns'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin/ProductsListing_' . date('YmdHis');
    }
}
