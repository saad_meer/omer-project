<?php

namespace App\DataTables\Admin;

use App\Models\Vendors;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class VendorListingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()->eloquent($query)
            ->addColumn('action', function ($row) {
                $profile_url = route('admin.loginAsVendor');
                return '<form method = "post" action = "' . $profile_url . '" >'.csrf_field().'
                          <button type = "submit" name = "vendors_id"
                           value = "' . $row->id . '" class="btn btn-outline-primary" > Login as</button >
                           </form >';})
            ->editColumn('status', function ($row) {
                if ($row->status) {
                    return '<span style="color: #1b55e2;
                               border: 2px dashed #1b55e2; background: transparent;" class="shadow-none badge badge-primary">Approved</span>';
                } else {
                    return '<span style="color: #1b55e2;
                                               border: 2px dashed #1b55e2; background: transparent;" class="shadow-none badge badge-danger">Suspended</span>';
                }
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin/VendorListingDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Vendors $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('admin/vendorlistingdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('id'),
            Column::make('add your columns'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin/VendorListing_' . date('YmdHis');
    }
}
