<?php

namespace App\DataTables\Admin;

use App\Models\Subscriptions;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class viewMembershipsListingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($row) {

                if ($row->status) {
                    if ($row->expiry < date('Y-m-d')) {
                        return '<span style="color: #e2a03f;
                               border: 2px dashed #e2a03f; background: transparent;"
                            class="shadow-none badge badge-danger">Expired!</span>';
                    } else {
                        return '<span style="color: #e2a03f;
                                 border: 2px dashed #e2a03f; background: transparent;" class="shadow-none badge badge-primary">Approved</span>';
                    }
                } else {
                    return '<span style="color: #e2a03f;
                          border: 2px dashed #e2a03f; background: transparent;" class="shadow-none badge badge-warning">Suspended</span>  ';
                }


            })
            ->editColumn('vendor_subscriptions_status', function ($row) {
                if ($row->status) {
                    if ($row->expiry < date('Y-m-d')) {
                        return '<button class="btn btn-outline-danger">Re-Sub</button>';
                    } else {
                        return '<button class="btn btn-outline-primary">View</button>';
                    }
                } else {
                    return '<button class="btn btn-outline-warning">Re-Activate</button> ';
                }
            })
            ->rawColumns(['action', 'vendor_subscriptions_status'])
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Admin/viewMembershipsListingDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Subscriptions $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('admin/viewmembershipslistingdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('id'),
            Column::make('add your columns'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Admin/viewMembershipsListing_' . date('YmdHis');
    }
}
