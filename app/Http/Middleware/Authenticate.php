<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

use Closure;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;


class Authenticate extends Middleware
{
    protected $guards;

    public function handle($request, Closure $next, ...$guards)
    {
        $this->guards = $guards;

        return parent::handle($request, $next, ...$guards);
    }


    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $last =  Arr::first($this->guards);
        if (! $request->expectsJson()) {

            if ($last === Config::get('global.guards.admin')) {
                return route('admin/login');
            }else if($last === Config::get('global.guards.vendor')){
                return route('login');
            }

            // Default
            return route('login');
        }
    }
}
