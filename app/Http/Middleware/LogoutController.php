<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Auth;

class LogoutController extends Controller
{
    use AuthenticatesUsers;

    public function Logout(Request $request)
    {
        $login_page = 'login';

        if ( Auth::guard(Config::get('constants.guards.admin'))->check()) {

            $this->guard(Config::get('constants.guards.admin'))->logout();

            $request->session()->invalidate();

            $login_page .=  '.admin';

        } else if(Auth::guard(Config::get('constants.guards.vendor'))->check() ) {

            $this->guard(Config::get('constants.guards.vendor'))->logout();

            $request->session()->invalidate();

            $login_page .=  '.vendor';
        }

        return redirect( route($login_page ));
    }

}
