<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;use Illuminate\Support\Facades\Config;


class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {

            if (Auth::guard($guard)->check()) {

                if ($guard === Config::get('global.guards.admin')) {
                    return redirect()->route('admin.dashboard');
                }
                else if ($guard === Config::get('global.guards.vendor')) {
                    return redirect()->route('vendor.dashboard');
                }

               // return redirect(RouteServiceProvider::HOME);
            }
        }

        return $next($request);
    }
}
