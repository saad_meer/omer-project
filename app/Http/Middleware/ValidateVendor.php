<?php

namespace App\Http\Middleware;

use App\Models\Subscriptions;
use App\Models\Vendors;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ValidateVendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Str::contains($request->route()->uri(),['vendor'])) {
            $vendor = Vendors::whereId(Auth::id())->first();
            if (!$vendor) {
                return redirect('vendor/create');
            }
        }

        if(!Str::contains($request->route()->uri(),['membership'])) {
//            $vendor = Vendors::where('user_id', Auth::id())->first();
            $vendor = Auth::user();
            if($vendor) {
                if (!$vendor->subscription) {
                    return redirect('membership/create');
                }

                if ($vendor->subscription->stripe_status != 'active') {
                    return redirect('membership');
                }

//                if ($vendor->subscription->expiry < date('Y-m-d')) {
//                    return redirect('subscription');
//                }
            }
        }

        return $next($request);
    }
}
