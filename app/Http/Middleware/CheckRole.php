<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ... $roles)
    {
        if(!$this->validateRoles($roles)) {
            abort(403);
        }

        return $next($request);
    }

    private function validateRoles($roles) {
        foreach ($roles as $role) {
            if (Auth::user()->hasRole($role))
                return true;
        }

        return false;
    }
}
