<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Auth;

class LogoutController extends Controller
{
    use AuthenticatesUsers;

    public function userLogout(Request $request)
    {
        $login_page = '';

        if ( Auth::guard(Config::get('global.guards.admin'))->check()) {

            $this->guard(Config::get('global.guards.admin'))->logout();

            $request->session()->invalidate();

            $login_page .=  'admin/login';

        } else if(Auth::guard(Config::get('global.guards.vendor'))->check() ) {

            $this->guard(Config::get('global.guards.vendor'))->logout();

            $request->session()->invalidate();

            $login_page .=  'login';
        }

        return redirect( route($login_page ));
    }

}
