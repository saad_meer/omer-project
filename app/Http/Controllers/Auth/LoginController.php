<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Auth;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('userLogout');
        $this->middleware('guest:vendor')->except('vendorLogout');
    }


    /**
     * @param Request $request
     * @param $guard
     * @return mixed
     */
    protected function guardLogin(Request $request, $guard)
    {
        // validate
        $this->validator($request);

        return Auth::guard($guard)->attempt(
            [
                'email' => $request->email,
                'password' => $request->password
            ],
            $request->get('remember')
        );
    }


    /**
     * Validate Request
     * @param Request $request
     * @return array
     */
    protected function validator(Request $request)
    {
        return $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:5'
        ]);
    }


    /**
     * Login Admin/User
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function userLogin(Request $request)
    {
        if ($this->guardLogin($request, Config::get('global.guards.admin'))) {
            return  redirect(route('admin.dashboard'));
        }

        return back()->withInput($request->only('email'));
    }

    /**
     * Login vendor
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function showVendorLogin()
    {
        return view('auth.login');
    }
    public function vendorLogin(Request $request)
    {
        if ($this->guardLogin($request,Config::get('global.guards.vendor'))) {
            return  redirect(route('vendor.dashboard'));
        }
        return back()->withInput($request->only('email'));
    }

    public function showAdminLoginForm()
    {
        return view('admin.login');
    }


}
