<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Subscriptions;
use App\Models\Categories;
use App\Models\User;
use App\Models\Vendors;
use App\Models\Widgets;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use App\DataTables\Admin\VendorListingDataTable;
use App\DataTables\Admin\CustomerListingDataTable;
use App\DataTables\Admin\OrdersListingDataTable;
use App\DataTables\Admin\ProductsListingDataTable;
use App\DataTables\Admin\ViewWidgetsListingDataTable;
use App\DataTables\Admin\viewMembershipsListingDataTable;
use App\DataTables\Admin\CategoryLisitngDataTAble;

use Illuminate\Support\Facades\Auth;

use App\Models\OrderItems;
use Session;
class AdminDashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }

    public function login()
    {
        if (!Auth::user()) {
            return view('admin.login');
        }

        if (Auth::user()->hasRole('Vendor')) {
            return redirect('/home');
        } elseif (Auth::user()->hasRole('Admin') or Auth::user()->hasRole('Superadmin')) {
            return redirect('/admin');
        }
    }

    public function vendorsList(Request $request, VendorListingDataTable $dataTable)
    {
        if ($request->ajax()) {
            //$query = Vendors::all();
            $query = Vendors::select('id', 'title', 'email', 'web_url', 'phone', 'about', 'status');
            return $dataTable->dataTable($query);
        }
        return view('admin.vendors');
    }

    public function customersList(Request $request, CustomerListingDataTable $dataTable)
    {

        if ($request->ajax()) {
            $query = Customers::select('customers.id as customers_id', 'vendors.title as vendor_company', 'customers.name', 'customers.email', 'customers.phone', 'customers.language', 'customers.payment_status', 'customers.is_guest', 'customers.status')
                ->join('vendors', 'vendors.id', 'customers.vendors_id');
            return $dataTable->dataTable($query);
        }
        $vendors = Vendors::all();
        return view('admin.customers')->with('vendors', $vendors);
    }


    public function ordersList(Request $request, OrdersListingDataTable $dataTable)
    {

        if ($request->ajax()) {
            $query = Orders::select('orders.id as orders_id', 'vendors.title as vendor_company', 'customers.name as customers_name', 'orders.shipping_total as orders_shipping_total', 'orders.order_total', 'orders.order_status')
                ->join('vendors', 'vendors.id', 'orders.vendors_id')
                ->join('customers', 'customers.id', 'orders.customers_id');
            return $dataTable->dataTable($query);
        }

        $vendors = Vendors::all();
        return view('admin.orders')->with('vendors', $vendors);
    }


    public function productsList(Request $request, ProductsListingDataTable $dataTable)
    {
        if ($request->ajax()) {
            //$query = Vendors::all();
            $query = Products::select('products.id as products_id', 'vendors.title as vendor_company', 'products.title as products_title', 'price', 'quantity', 'image_url', 'products.status as products_status')
                ->join('vendors', 'vendors.id', 'products.vendor_id');
            return $dataTable->dataTable($query);
        }

        $vendors = Vendors::all();

        return view('admin.products')->with('vendors', $vendors);
    }


    public function viewWidgets(Request $request, ViewWidgetsListingDataTable $dataTable)
    {
        if ($request->ajax()) {
            //$query = Vendors::all();
            $query = Widgets::select('widgets.id as widgets_id', 'widgets.title as widgets_title', 'widgets.description',
                'widgets.width', 'widgets.height', 'widgets.token',
                'vendors.title as vendor_company', 'widgets.status as widgets_status', 'widgets.created_at', 'widgets.updated_at')
                ->join('vendors', 'vendors.id', 'widgets.vendors_id');
            return $dataTable->dataTable($query);
        }

        $vendors = Vendors::all();

        return view('admin.widgets')
            ->with('vendors', $vendors);
    }


    public function viewMemberships(Request $request, viewMembershipsListingDataTable $dataTable)
    {

        if ($request->ajax()) {
            //$query = Vendors::all();
            //expiry column not found somone put today date instead
            $query = Subscriptions::select('vendor_subscriptions.id as vendor_subscriptions_id',
                'vendors.title as vendor_company',
                'vendor_subscriptions.level as vendor_subscriptions_level',
                'vendor_subscriptions.stripe_price',
                'vendor_subscriptions.period as subscriptions_period', 'vendor_subscriptions.paid_by',
                'vendor_subscriptions.status as vendor_subscriptions_status',
                'vendor_subscriptions.created_at', 'vendor_subscriptions.updated_at')
                ->join('vendors', 'vendors.id', 'vendor_subscriptions.vendors_id');
            return $dataTable->dataTable($query);
        }

        $vendors = Vendors::all();

        return view('admin.subscriptions')
            ->with('vendors', $vendors);
    }



      public function adminProfile(){
          $user_data = User::where('users.id', Auth::user()->id)->get()->first();

          return view('admin.adminprofile')  ->with('admin', $user_data);
      }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function AdminProfileUpdate(Request $request){



        $this->validate($request, [
            'upload_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
         //   'upload_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
        ]);

        $image = $request->file('upload_image');


        $new_name = rand() . '.' . $image->getClientOriginalExtension();

        $image->move(public_path('Uploads/AdminProfile'), $new_name);
        $new_image_path='Uploads/AdminProfile/'.$new_name;


        User::where('users.id', Auth::user()->id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
              //  'password' => 1,
                'profile_photo_path' => $new_image_path,
            ]);


        $user_data = User::where('users.id', Auth::user()->id)->get()->first();
        return back()->with('admin', $user_data);
       // return view('admin.adminprofile')->with('admin', $user_data);
    }


    /**
     * Login as vendor.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function loginAsVendor(Request $request)
    {
        $vendor = Vendors::find(trim(request('vendors_id')));
        // dd($vendor);
        //   $user = User::find($vendor->user_id);
        Auth::logout();
        Auth::guard('vendor')->login($vendor);

        return redirect('vendor/dashboard');
    }

    //related to main Dashboard

    public function cart()

    {

        return view('cart');

    }

    public function addToCart($id)
    {
        $product = Products::findOrFail($id);

        $vendor_id = session()->get('vendor_id');
        if(isset($vendor_id))
        {
            if($vendor_id!= $product->vendor_id)
            {

                Session::forget('cart');
                session()->put('vendor_id', $product->vendor_id);
            }
        }
        else
        {
            session()->put('vendor_id', $product->vendor_id);
            $vendor_id=session()->get('vendor_id');

        }

        $cart = session()->get('cart', []);

        if(isset($cart[$id])) {

            $cart[$id]['quantity']++;

        } else {

            $cart[$id] = [

                "name" => $product->title,
                "quantity" => 1,
                "price" => $product->price,
                "image" => $product->image_url
            ];
        }
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function updateCart(Request $request)
    {
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function removeCart(Request $request)

    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function checkout()
    {
        return view('checkout');
    }



    public function productsVendor(){
        $vendors = Vendors::all();

        return view('vendorlisting')->with('vendors',$vendors);
    }


    public function productData(Request $request){
        $singleProducts = Products::where('products.id', $request->id)->get()->first();
        if (!empty($singleProducts)) {
            echo json_encode(array("data" => 1, 'product_data' => $singleProducts));

        } else {
            echo json_encode(array("data" => 0));
        }
    }

    public function addToCartAjax(Request $request){

      //  dd($request);
        dd($request->price . ' ' .$request->quantity . ' ' . $request->instructions);
        dd($request->quantity);
        dd($request->instructions);
    }

    public function vendorLists($id){


        $vendors = Vendors::all();
        $Products =Products::select('products.id as products_id','category_id','categories.categoryTitle as category', 'products.title as products_company',
            'products.price', 'products.quantity', 'products.image_url','vendors.id as vendor_id', 'vendors.name as vendor_name','vendors.title as vendor_title')
            ->leftJoin('categories', 'products.category_id', 'categories.id')
            ->join('vendors', 'vendors.id', 'products.vendor_id')
            ->where('products.vendor_id',$id)->get();

        $categories = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('products.vendor_id',$id)
            ->DISTINCT('products.category_id')
            ->select( 'products.category_id','categoryTitle','products.vendor_id')
            ->get();



        return view('productlisting')->with('vendors',$vendors)->with('products',$Products)->with('categories',$categories)->with('selectedVendor',$id);
    }

    public function vendorLists2 ($id,$category_id){


        $vendors = Vendors::all();
        $Products =Products::select('products.id as products_id','category_id','categories.categoryTitle as category', 'products.title as products_company',
            'products.price', 'products.quantity', 'products.image_url','vendors.id as vendor_id', 'vendors.name as vendor_name','vendors.title as vendor_title')
            ->leftJoin('categories', 'products.category_id', 'categories.id')
            ->join('vendors', 'vendors.id', 'products.vendor_id')
            ->where('products.vendor_id',$id)
            ->where('products.category_id',$category_id)
            ->get();

        $categories = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('products.vendor_id',$id)
            ->DISTINCT('products.category_id')
            ->select( 'products.category_id','categoryTitle','products.vendor_id')
            ->get();



        return view('productlisting')->with('vendors',$vendors)->with('products',$Products)->with('categories',$categories);
    }

    public function categoryList (){

    }


}
