<?php

namespace App\Http\Controllers;

use App\Models\MyClub;
use Illuminate\Http\Request;
use App\DataTables\Vendor\ClubsDataTable;
use Illuminate\Support\Facades\Auth;
use App\Models\OrderItems;
use App\Models\Orders;
use Validator;
use Session;
use App\Jobs\SendEmailJob;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction_id'         => 'required',
            'transaction_status'        => 'required', 
        ]);
          if($validator->fails()){
               
                return json_encode(array('code' => 404,'record' => $validator->errors()));
            }
            else
            {
                $total =0;
                $vendor_id = session()->get('vendor_id'); 
                $order = new Orders;
                $order->vendors_id=isset($vendor_id)?$vendor_id:5;
                $order->transaction_id=$request->transaction_id;
                $order->transaction_status=$request->transaction_status;
                $order->billing_first_name=$request->first_name;
                $order->billing_last_name=$request->last_name;
                $order->billing_email=$request->email;
                $order->billing_phone=$request->phone_num;
                $order->save();
                $order_array=array();
                $order_array=[
                    'vendors_id'=>isset($vendor_id)?$vendor_id:5,
                    'transaction_id'=>$request->transaction_id,
                    'transaction_status'=>$request->transaction_status,
                    'billing_first_name'=>$request->first_name,
                    'billing_last_name'=>$request->last_name,
                    'billing_email'=>$request->email,
                    'billing_phone'=>$request->phone_num,
                ];
                $cart = session()->get('cart', []);
                foreach( $cart as $id => $details)
                {
                    $total += $details['price'] * $details['quantity'];
                    $order_item = new OrderItems();
                    $order_item->orders_id = $order->id;
                    $order_item->product_id = $id;
                    $order_item->product_name = $details['name'];
                    $order_item->quantity = $details['quantity'];
                    $order_item->total = $details['price'] * $details['quantity'];
                    $order_item->save(); 

                }
                Orders::where('id',$order->id)->update(['order_total'=> $total]);
                Session::forget('vendor_id');
                Session::forget('cart');
                $data = ['email'=>$request->email, 'subject'=>'Order Information','order_item'=> $order_array];
                dispatch(new SendEmailJob($data));
                return json_encode(array('code' => 200));
            }
    } 
    public function index(Request $request , ClubsDataTable $dataTable)    {

        if ($request->ajax()) {
                    $query = MyClub::where('myclub.vendors_id', Auth::user()->id)->select('myclub.id', 'vendors.name as vendor_name', 'myclub.club_title', 'myclub.number_of_products', 'myclub.club_discount')
                        ->join('vendors', 'vendors.id', 'myclub.vendors_id');
                    return $dataTable->dataTable($query);
         }
                return view('vendor.club');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.addclub');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'club_title' => 'required',
            'number_of_products' => 'required',
            'club_discount' => 'required|integer|between:1,100',
        ]);
        $input=$request->all();
        if ($request->hasFile('club_image_url'))
        {

            $file = $request->file('club_image_url');
            $fileName = time().'_'.$file->getClientOriginalName();


            $request->file('club_image_url')->move(public_path('images'), $fileName);

            $input['club_image_url']='images/'. $fileName;
        }

        $input['vendors_id']=Auth::user()->id;

        $my_club=MyClub::create($input);
        if($my_club)
        {

            return redirect('myclub')->with('message','Club Created Succesfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id)
        {
            $club = MyClub::find($id);
            if($club)
            {
                $club->delete();
            }
        }

        //return redirect('myclub');
    }
}
