<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\OrderSchedules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VendorDashboardController extends Controller
{
    public function index() {

        $totalOrders = Orders::where('vendors_id', Auth::user()->id)->count();
        $scheduledOrders = OrderSchedules::where('vendors_id', Auth::user()->id)->count();
        $totalRevenue = Orders::where('vendors_id', Auth::user()->id)->sum('order_total');
        $orders = Orders::with('customer')->where('vendors_id', Auth::user()->id)->orderBy('created_at', 'desc')->offset(0)->limit(10)->get();
        $orderSchedules = OrderSchedules::with('order')->where('vendors_id', Auth::user()->id)->orderBy('created_at', 'asc')->offset(0)->limit(10)->get();

        return view('vendor.dashboard')
            ->with('scheduledOrders', $scheduledOrders)
            ->with('totalOrders', $totalOrders)
            ->with('totalRevenue', $totalRevenue)
            ->with('orders', $orders)
            ->with('orderSchedules', $orderSchedules);
    }

    public function login() {
        return view('vendor.login');
    }
}
