<?php

namespace App\Http\Controllers;

use App\Models\DefaultSubscriptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\StripeClient;

class DefaultSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stripe = new StripeClient(env('STRIPE_SECRET'));
        $plansRaw = $stripe->plans->all(['limit' => 3, 'product' => 'prod_K3LDu6LNBFZUEI']);
        $plans = $plansRaw->data;
        $defaultPlans = DefaultSubscriptions::limit(3)->get();

        return view('admin.default_subscriptions')
            ->with('defaultPlans', $defaultPlans)
            ->with('plans', $plans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subscription = DefaultSubscriptions::find($id);
        $subscription->price = $request->price;
        $subscription->description = $request->description;
        $subscription->features = $request->features;
        $subscription->save();

        return redirect(route('admin.defaultMemberships.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
