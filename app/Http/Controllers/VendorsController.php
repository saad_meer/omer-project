<?php

namespace App\Http\Controllers;

use App\Models\DefaultWidget;
use App\Models\Products;
use App\Models\Vendors;
use App\Models\Widgets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class VendorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vendor.dashboard');
    }

    public function updateSettings(){
        $vendor = Vendors::find(Auth::user()->id);
        return view('vendor.edit')->with('vendor', $vendor);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendor.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request->validate([
                'email' => 'required|email|unique:vendors|max:100',
                'title' => 'required',
                'web_url' => 'required',
                'phone' => 'required'
            ]);


/*
        $this->validate($request, [
            'profile_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
            //   'upload_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
        ]);*/
      //  dd($request);
        $image = $request->file('profile_image');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();

        $image->move(public_path('Uploads/vedor_profile/'), $new_name);
        $new_image_path='Uploads/vedor_profile/'.$new_name;




        $vendor = new Vendors();
            $vendor->user_id = Auth::id();
            $vendor->title = request('title');
            $vendor->email = request('email');
            $vendor->web_url = request('web_url');
            $vendor->phone = request('phone');
            $vendor->address = request('address');
            $vendor->about = request('about');
            $vendor->profile_image = $new_image_path;
            $vendor->client_id		=   substr(md5('012345678abcdefghijklmnopqrstuvwxyz'), 0, 10);  // md5(rand());//md5('abcdefghijklmnopqrstuvwxyz', Str::random(8));
            $vendor->access_token =   Auth::user()->createToken('members-one')->plainTextToken;

            if ($vendor->save()) {
                $defaultWidget = DefaultWidget::find(1);
                $widget = new Widgets();
                $widget->title = $defaultWidget->title;
                $widget->description = $defaultWidget->description;
                $widget->width = $defaultWidget->width;
                $widget->height = $defaultWidget->height;
                $widget->content = $defaultWidget->content;
                $widget->token = hash('sha256', Str::random(60));
                $vendor->widget()->save($widget);
            }

            return redirect('membership/create');
    }

    public function updateVendorSettings(Request $request){


        $request->validate([
            'email' => 'required|email|unique:vendors,email,'.Auth::user()->id.',id',
            'title' => 'required',
          //  'web_url' => 'required',
            'phone' => 'required'
        ]);

       /* $this->validate($request, [
            'profile_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
            //   'upload_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
        ]);*/

        $image = $request->file('profile_image');

        if(!empty($image)){
            $new_name = rand() . '.' . $image->getClientOriginalExtension();

            $image->move(public_path('Uploads/vedor_profile/'), $new_name);
            $new_image_path='Uploads/vedor_profile/'.$new_name;
        }
        else{
            $new_image_path='';
        }



        if($new_image_path != '')
        {
            Vendors::where('id',Auth::user()->id)
                ->update([
                    'email' => $request->email,
                    'title' => $request->title,
                    'phone' => $request->phone,
                    'profile_image' => $new_image_path
                ]);
        }else{
            Vendors::where('id',Auth::user()->id)
                ->update([
                    'title' => $request->title,
                    'status' => $request->status,
                    'phone' => $request->phone,
                ]);
        }




        $vendor = Vendors::find(Auth::user()->id);

        return redirect()->back()->with('message', 'Successfully Updated!')->with('vendor', $vendor);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
         //   'email' => 'required|email|unique:vendors|max:100',
            'email' => 'required|max:100|unique:vendors,email,'.$id.',id',
            'title' => 'required',
            'web_url' => 'required|url',
            'phone' => 'required|numeric'
        ]);

        $vendor = Vendors::findOrNew($id);
        $vendor->fill($request->all());
        $vendor->save();

        return redirect()->back()->with('message', 'Successfully Updated!');
        //return redirect('/updateSettings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function updateAccessToken($id)
    {
        $vendor = Vendors::find($id);
        if($vendor)
        {
            Auth::user()->tokens()->delete();
            $vendor->client_id		=   substr(md5('0123456789abcdefghijklmnopqrstuvwxyz'), 0, 10);
            $vendor->access_token   =   Auth::user()->createToken('members-one')->plainTextToken;
            $vendor->save();
        }
        return redirect('/pluginSetting');
    }
    public function pluginSetting()
    {
        $vendor = Vendors::find(Auth::user()->id)->first();

        return view('vendor/pluginsetting')->with('vendor', $vendor);
    }
}
