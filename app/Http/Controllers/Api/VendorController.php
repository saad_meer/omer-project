<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vendors;
use Validator;

class VendorController extends BaseController
{
    public function validateVendor(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'moiVendorId'=> 'required',
                'moiVendorSecretToken'=> 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),401);
            }

            $vendor=Vendors::whereClientId($request->moiVendorId)->first();
            if($vendor)
            {
                if($vendor->access_token==$request->moiVendorSecretToken)
                {
                    return $this->sendResponse([],'Configurations saved successfully');
                }
                else
                {
                    return $this->sendError('Token Not Matched',[],404);
                }
            }
            return $this->sendError('Vendor ID Not Found',[],404);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),[] ,500);
        }
    }
}
