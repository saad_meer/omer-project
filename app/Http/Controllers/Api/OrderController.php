<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Orders;
use App\Models\OrderItems;
use App\Models\Customers;
use App\Models\OrderSchedules;
use App\Models\Package;
use App\Models\PackageItem;
use App\Models\PackageShipping;
use App\Models\Products;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\MyClub; 

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        try{
            $validator = Validator::make($request->all(), [
                'schedule_type'         => 'required',
                'schedule_value'        => 'required',
                'orders_id'             => 'required',
                'order_items'           => 'required',
                'schedule_type'         => 'required',
                'schedule_value'        => 'required',
            ]);
            //
            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),401);
            }
            $input=$request->all();
            $get_order=Orders::where('orders_id',$input['orders_id'])->first();
            // if($get_order)
            // {
            //     return $this->sendError('Order Already Created',401);
            // }
            DB::beginTransaction();

            $customer=json_decode($input['customer_data'],true);
            $package_schedule=json_decode($input['package_schedule'],true);

            if(!empty($customer))
            {
                $get_customer=Customers::where('email',$customer['email'])->first();

                if(!$get_customer)
                {
                    $stripe = new \Stripe\StripeClient(
                        env('STRIPE_SECRET')
                    );
                    $strip_customer_id= $stripe->customers->create([
                        'email' =>$customer['email'],
                        'name' => $customer['name'],
                        'payment_method'=>$package_schedule['stripe_payment_method']
                    ]);

                    $get_customer=new Customers();
                    $get_customer->vendors_id=Auth::id();
                    $get_customer->name=$customer['name'];
                    $get_customer->email=$customer['email'];
                    $get_customer->customers_id=$customer['id'];
                    $get_customer->stripe_customer_id= $strip_customer_id['id'];
                    $get_customer->save();
                }
            }
            else
            {
                $this->sendError('Customer Not Found',[],500);
            }
            $input['customers_id']=$get_customer->id;
            $input['vendors_id']=Auth::id();
            $order = Orders::create($input);
            $input['orders_id']=$order->id;
            $orders_chedules = OrderSchedules::create($input);

            if(!$order) {
                $this->sendError('Something went wrong',[],500);
            }
            $order_items=json_decode($input['order_items'],true);
            if(!empty($order_items))
            {
                foreach($order_items as $order_item)
                {
                    $order_item['vendors_id']=Auth::id();
                    $order_item['orders_id']=$order->id;
                    $order = OrderItems::create($order_item);
                }
            }

            // create package

            if(!empty($package_schedule))
            {
                $stripe = new \Stripe\StripeClient(
                    env('STRIPE_SECRET')
                  );
                  $stripe->paymentMethods->attach(
                    $package_schedule['stripe_payment_method'],
                    ['customer' =>  $get_customer->stripe_customer_id]
                  );
            }
            //save  package
            $package_schedule_save=new Package();
            $package_schedule_save->customers_id=$get_customer->id;
            $package_schedule_save->vendors_id=Auth::id();
            $package_schedule_save->orders_id=$package_schedule['orders_id'];
            $package_schedule_save->schedule_type=$package_schedule['schedule_type'];
            $package_schedule_save->schedule_value=$package_schedule['schedule_value'];
            $package_schedule_save->stripe_customer_id= $get_customer->stripe_customer_id;
            $package_schedule_save->stripe_payment_method=$package_schedule['stripe_payment_method'];
            $package_schedule_save->stripe_setup_intent_id=$package_schedule['stripe_setup_intent_id'];
            $package_schedule_save->save();

            $package_shipping_info=json_decode($input['package_shipping_info'],true);
            if(!empty($package_shipping_info))
            {
                $package_shipping_info_save                  =  new PackageShipping();
                $package_shipping_info_save->customers_id    =  $get_customer->id;
                $package_shipping_info_save->vendors_id      =  Auth::id();
                $package_shipping_info_save->package_id      =  $package_schedule_save->id;
                $package_shipping_info_save->first_name      =  $package_shipping_info['first_name'];
                $package_shipping_info_save->last_name       =  $package_shipping_info['last_name'];
                $package_shipping_info_save->compnay         =  $package_shipping_info['company'];
                $package_shipping_info_save->email           =  $package_shipping_info['email'];
                $package_shipping_info_save->phone           =  $package_shipping_info['phone'];
                $package_shipping_info_save->country         =  $package_shipping_info['country'];
                $package_shipping_info_save->state           =  $package_shipping_info['state'];
                $package_shipping_info_save->city            =  $package_shipping_info['city'];
                $package_shipping_info_save->postcode        =  $package_shipping_info['postcode'];
                $package_shipping_info_save->address_line_1  =  $package_shipping_info['address_1'];
                $package_shipping_info_save->address_line_2  =  $package_shipping_info['address_2'];
                $package_shipping_info_save->save();
            }
            $package_item=json_decode($input['package_item'],true);
            if(!empty($package_item))
            {
                foreach($package_item as $package_product)
                {
                    $package_item                              =  new PackageItem();
                    $package_item->package_id                  =  $package_schedule_save->id;
                    $package_item->product_id                  =  Products::get_product_id($package_product['woocomerce_product_id']);
                    $package_item->woocomerce_product_id       =  $package_product['woocomerce_product_id'];
                    $package_item->title                       =  $package_product['title'];
                    $package_item->image_url                   =  $package_product['image_url'];
                    $package_item->price                       =  $package_product['price'];
                    $package_item->save();
                }
            }
            // $product_array_member=json_decode($input['product_array_member'],true);
            // if(!empty($product_array_member))
            // {
            //     foreach($product_array_member as $product_id)
            //     {
            //         // create pacakge items
            //         $package_item=new PackageItem();
            //         $package_item->product_id=$product_id;
            //         $package_item->package_id=$package->id;
            //         $package_item->save();
            //     }
            // }
            DB::commit();
            return $this->sendResponse($order);

        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }
    }
    public function get_customer_orders(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'email'      => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),401);
            }
            $customer=Customers::where('email',$request->email)->first();
            if($customer)
            {
               $orders=Orders::where('customers_id',$customer->id)->get();
               if(!empty($orders) && count($orders)>0)
               {
                   return $this->sendResponse($orders);
               }
               else
               {
                return  $this->sendError('Order Not Found',[],500);
               }
            }
           return  $this->sendError('Customer Not Found',[],500);
        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }
    }
 
    public function get_vendors_clubs()
    {
        try{

            $clubs=MyClub::where('vendors_id',Auth::id())->get();
            if(!empty($clubs) && count($clubs)>0)
            {
                return $this->sendResponse($clubs);
            }
           return  $this->sendError('Clubs Not Found',[],500);
        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
