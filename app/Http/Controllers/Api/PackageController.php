<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator; 
use App\Models\Customers; 
use App\Models\Package;
use App\Models\PackageItem;
use App\Models\PackageShipping; 
use Illuminate\Support\Facades\Auth;
use DB;
use App\Models\MyClub; 
use App\Http\Traits\PackageHelper;

class PackageController extends BaseController
{
    use PackageHelper;

    public function saveSchedule(Request $request)
    {
        $input=$request->all();
        try{
            $validator = Validator::make($request->all(), [
                'schedule_type'         => 'required',
                'schedule_value'        => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),401);
            }
            $input=$request->all();
            if(isset($input['package_id']))
            {
                Package::where('id',$input['package_id'])->delete();
            }

            DB::beginTransaction();

            $customer=json_decode($input['customer_data'],true);
            $package_schedule=json_decode($input['package_schedule'],true);

            if(!empty($customer))
            {
              $get_customer=$this->createCustomer($customer,$package_schedule);
            }
            else
            {
               return $this->sendError('Customer Not Found',[],500);
            }

            // create package
            if(!empty($package_schedule))
            {
                $this->attachStripe($package_schedule,$get_customer);
            }
            $package_schedule_save=$this->packageStore($package_schedule,$get_customer);
            
            $package_shipping_info=json_decode($input['package_shipping_info'],true);
            if(!empty($package_shipping_info))
            {
                $this->PackageShippingInfo($package_shipping_info,$package_schedule_save,$get_customer);
            }
            $package_item=json_decode($input['package_item'],true);
            if(!empty($package_item))
            {
              $this->PackageSave($package_item,$package_schedule_save);
            }
            DB::commit();
            return $this->sendResponse($package_schedule_save);

        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage());
        }
    }
}
