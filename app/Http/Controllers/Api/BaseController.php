<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    const PHONE_VALIDATION_RULE = 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10';
    CONST PER_PAGE = 20;
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result=[], $message='Success')
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($errorMessage, $errorData = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $errorMessage,
        ];
        if(!empty($errorData)){
            $response['data'] = $errorData;
        }

        return response()->json($response, $code);
    }

    public function sendException($exception) {
        switch ($exception->getCode()){
            case 23000:
                return $this->sendError('Exception');
                break;
            default:
                return $this->sendError($exception->getMessage());
        }
    }

    public function applyPagination($request, $db_query){
        $offset = $request->has('page') && is_numeric($request->page) ? $request->page : 1;
        $limit =  ($request->has('per_page') && is_numeric($request->per_page) && $request->per_page <= self::PER_PAGE )
            ? intval($request->per_page) : self::PER_PAGE;
        $db_query->skip(($offset-1 ) * $limit)->take($limit);

        return $db_query;
    }

    public function applySorting($request, $db_query, $sortBy = 'id'){
        if(isset($request->sort) &&  $request->filled('sort')) {
            $db_query->orderBy($sortBy, $request->sort);
        }

        return $db_query;
    }

    public function isOfferExpired($expiry){
        $now = date("Y-m-d H:i:s");

        return $expiry < $now;
    }
}
