<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customers;
use App\Models\Vendors;
use App\Models\PackageShipping;
use App\Models\Package;
use App\Models\MyClub;
use Validator;
use Illuminate\Support\Facades\Auth;
use Stripe\StripeClient;
use Laravel\Cashier\Exceptions\IncompletePayment; 

class CustomerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function checkRegisterCustomer(Request $request)
    {
        try{
            $user = Auth::user();
            $customer = Customers::where('email',$request->customer_email)->first();
            if(!empty($customer))
            {

                $intent = $user->createSetupIntent();
                return  $this->sendResponse(['is_exist'=>1,'intent'=>  $intent->client_secret]);
            }
            else
            {

                $intent = $user->createSetupIntent();

                return  $this->sendResponse(['is_exist'=>0,'intent'=> $intent->client_secret]);
            }


        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }

    }
    public function get_customer_address(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'email'      => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),401);
            }
            $customer = Customers::where('email',$request->email)->first();
            if($customer)
            {
                $customer_subscribe_package = MyClub::whereHas('package', function($q) use ($customer) {
                    $q->where('customers_id',$customer->id);
                })->whereVendorsId(Auth::id())->get();
                
              // $customer_subscribe_package = MyClub::with('myclub')->where('customers_id',$customer->id)->get();
               if(!empty($customer_subscribe_package) && count($customer_subscribe_package)>0)
               {
                    return $this->sendResponse($customer_subscribe_package);
               }
               return  $this->sendError('Clubs Not Found',[],500);
            }
           return  $this->sendError('Customer Not Found',[],500);
        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }
    }
    public function get_customer_packages(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'email'      => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),401);
            }
            $customer=Customers::where('email',$request->email)->first();
            if($customer)
            {
               $package=Package::with('myclub:id,club_title')
                                ->with('packageItem')->where('customers_id',$customer->id)->get();
               if(!empty($package) && count($package)>0)
               {
                   return $this->sendResponse($package);
               }
               else
               {
                return  $this->sendError('Package Not Found',[],500);
               }
            }
           return  $this->sendError('Customer Not Found',[],500);
        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'customers_id'      => 'required',
                'name'              => 'required',
                'email'             => 'required|unique:customers,email',
                'setup_intent_id'   =>  'required',
                'payment_method'    => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors(),401);
            }
                $customer=$request->all();

                $stripe = new \Stripe\StripeClient(
                    env('STRIPE_SECRET')
                );
                $strip_customer_id= $stripe->customers->create([
                    'email' =>$customer['email'],
                    'name' => $customer['name'],
                    'payment_method'=>$customer['payment_method']
                ]);
                $stripe = new \Stripe\StripeClient(
                    env('STRIPE_SECRET')
                  );
                  $stripe->paymentMethods->attach(
                    $customer['payment_method'],
                    ['customer' => $strip_customer_id['id']]
                  );



            $get_customer=new Customers();
            $get_customer->vendors_id=Auth::id();
            $get_customer->name=$customer['name'];
            $get_customer->email=$customer['email'];
            $get_customer->stripe_customer_id=$strip_customer_id['id'];
            $get_customer->stripe_payment_method_id=$customer['payment_method'];
            $get_customer->stripe_setup_intent_id=$customer['setup_intent_id'];
            $get_customer->customers_id=$customer['customers_id'];
            $get_customer->save();

            return $this->sendResponse($get_customer);
        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
