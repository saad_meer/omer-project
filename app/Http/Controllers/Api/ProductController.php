<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Products;
use Illuminate\Support\Facades\Auth;


class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $product=Products::where('vendor_id',Auth::id())->get();
            if(!empty($product))
            {
                return $this->sendResponse($product);
            }
            return $this->sendError('Product Not Found',[],404);
        } catch (\Exception $exception) {
            return $this->sendError($exception->getMessage(),[] ,500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $input=$request->all();
            $product_exsist=Products::where('product_id',$input['product_id'])->first();
            if($product_exsist)
            {
                Products::where('product_id',$input['product_id'])->update($input);
                $product= Products::where('vendor_id',Auth::id())->get();
                return $this->sendResponse($product);
            }
            $input['vendor_id']=Auth::id();
            $product = Products::create($input);
            if(!$product) {
                $this->sendError('Something went wrong',[],500);
            }
            $product= Products::where('vendor_id',Auth::id())->get();
            return $this->sendResponse($product);

        } catch (\Illuminate\Database\QueryException $exception) {
            return $this->sendException($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   
}
