<?php

namespace App\Http\Controllers;
use Validator;
use App\DataTables\Vendor\CategoryLisitngDataTAble;
use App\Models\Categories;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    //

    public function categoryList(Request $request, CategoryLisitngDataTAble $dataTable)
    {
        if ($request->ajax()) {

            $query = Categories::select('id', 'categoryTitle');
            return $dataTable->dataTable($query);
        }

        return view('admin.categories');
    }

    public function storeCategory(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'category_title' => 'required',
            'category_slug'  => 'required',
        ]);

        $error_array = array();
        $success_output = '';
        if ($validation->fails())
        {
            foreach($validation->messages()->getMessages() as $field_name => $messages)
            {
                $error_array[] = $messages;
            }
        }
        else
        {
            if($request->get('button_action') == "insert")
            {
                $Category = new Categories([
                    'categoryTitle'    =>  $request->get('category_title'),
                    'categorySlug'     =>  $request->get('category_title')
                ]);
                $Category->save();
                $success_output = '<div class="alert alert-success">Data Inserted</div>';
            }

            if($request->get('button_action') == 'update')
            {
                $Category = Categories::find($request->get('category_id'));
                $Category->categoryTitle = $request->get('category_title');
                $Category->categorySlug = $request->get('category_slug');
                $Category->save();
                $success_output = '<div class="alert alert-success">Data Updated</div>';
            }
        }
        $output = array(
            'error'     =>  $error_array,
            'success'   =>  $success_output
        );
        echo json_encode($output);
    }

    function showCategory(Request $request)
    {
        $id = $request->input('id');
        $Category = Categories::find($id);
        $output = array(
            'categoryTitle'    =>  $Category->categoryTitle,
            'categorySlug'     =>  $Category->categorySlug
        );
        echo json_encode($output);
    }

    public function removeCategory(Request $request){
        $Category = Categories::find($request->input('id'));
        if($Category->delete())
        {
            echo 'Data Deleted';
        }
    }

}
