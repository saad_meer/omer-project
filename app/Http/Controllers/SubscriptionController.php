<?php

namespace App\Http\Controllers;

use App\Models\DefaultSubscriptions;
use App\Models\Subscriptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Stripe\StripeClient;
use Laravel\Cashier\Exceptions\IncompletePayment;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscription =  Auth::user()->subscription;
        if (!$subscription){

            return redirect(route('membership.create'));
        }

        $stripe = new StripeClient(env('STRIPE_SECRET'));
        $plansRaw = $stripe->plans->all(['limit' => 3, 'product' => 'prod_K3LDu6LNBFZUEI']);
        $plans = $plansRaw->data;
        $defaultPlans = DefaultSubscriptions::limit(3)->get();
        return view('vendor.membership')
            ->with('defaultPlans', $defaultPlans)
            ->with('plans', $plans)
            ->with('subscription', $subscription)
            ->with('intent', Auth::user()->createSetupIntent());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $stripe = new StripeClient(env('STRIPE_SECRET'));
        $plansRaw = $stripe->plans->all(['limit' => 3, 'product' => 'prod_K3LDu6LNBFZUEI']);
        $plans = $plansRaw->data;
        $defaultPlans = DefaultSubscriptions::limit(3)->get();

        return view('vendor.membership')
            ->with('defaultPlans', $defaultPlans)
            ->with('plans', $plans)
            ->with('intent', Auth::user()->createSetupIntent());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->saveSubscription($request);

        return redirect()->route('vendor.dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->saveSubscription($request);

        return redirect('/membership');
    }

    private function saveSubscription(Request $request) {
        $vendor = Auth::user();
        try {
            $vendor->newSubscription('default', trim($request->plan))->create(trim($request->stripe_pm),
                ['email' => $vendor->email]
            );

            return redirect('vendor/dashboard');
        } catch (IncompletePayment $exception) {
            return redirect()->route(
                'membership',
                [$exception->payment->id, 'redirect' => back()->with('error',$exception->getMessage())]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
