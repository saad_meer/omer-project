<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use Illuminate\Http\Request;
use App\DataTables\Vendor\CustomerDataTable;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index1()
    {
        $customers = Customers::where('vendors_id', Auth::user()->id)->get();

        return view('vendor.customers')->with('customers', $customers);
    }

    public function index(Request $request , CustomerDataTable $dataTable)
        {
            if ($request->ajax()) {
                               $query = Customers::where('vendors_id', Auth::user()->id)->select('customers.id', 'vendors.name as vendor_name', 'customers.name', 'customers.email', 'customers.phone'
                                   , 'customers.language', 'customers.payment_status', 'customers.is_guest', 'customers.status')
                                   ->join('vendors', 'vendors.id', 'customers.vendors_id');
                               return $dataTable->dataTable($query);
                    }
                           return view('vendor.customers');
        }

    public function getAll() {

        return view('admin.customers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd($id);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
