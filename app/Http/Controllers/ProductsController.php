<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;
use App\DataTables\Vendor\ProductsDataTable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index1()
    {
        $products = Products::with('vendor:id,name')->where('vendor_id', Auth::user()->id)->get();

        return view('vendor.products')->with('products', $products);
    }*/
    public function index(Request $request , ProductsDataTable $dataTable)
    {
        if ($request->ajax()) {
                            $query = Products::where('vendor_id', Auth::user()->id)->select('vendors.id as vendorID','products.id','categories.categoryTitle as category', 'vendors.name as vendor_name', 'products.title', 'products.price','products.quantity','products.image_url','products.status')
                                ->leftJoin('categories', 'products.category_id', 'categories.id')
                                ->leftJoin('vendors', 'vendors.id', 'products.vendor_id');
                            return $dataTable->dataTable($query);
                 }
        return view('vendor.products');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $Categories = Categories::all();

        return view('vendor.productsCreate')->with('Categories', $Categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd(Str::random(8));
        $Products = new Products;

        $this->validate($request, [
            'image_url'  => 'required|image|mimes:jpg,png,gif|max:2048'
            //   'upload_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
        ]);

        $image = $request->file('image_url');
        $product_id =  Str::random(10);

        $new_name = rand() . '.' . $image->getClientOriginalExtension();

        $image->move(public_path('Uploads/Products/'), $new_name);
        $new_image_path='Uploads/Products/'.$new_name;


        $Products->title = $request->title;

        $Products->status = $request->status;
        $Products->category_id = $request->category_id;
        $Products->description = $request->description;
        $Products->short_description = $request->short_description;
        $Products->price = $request->price;
        $Products->quantity = $request->quantity;

        $Products->vendor_id = Auth::user()->id;
        $Products->image_url = $new_image_path;
        $Products->product_id = $product_id;

        $Products->save();
        return redirect('products');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Products = Products::find($id);
        $Categories = Categories::all();
        $output = array(
            'id' =>  $Products->id,
            'title'    =>  $Products->title,
            'status'     =>  $Products->status,
            'description'     =>  $Products->description,
            'category_id'     =>  $Products->category_id,
            'short_description'     =>  $Products->short_description,
            'price'     =>  $Products->price,
            'quantity'     =>  $Products->quantity,
            'image_url'     =>  $Products->image_url,

        );



        return view('vendor.productsCreate')->with('Categories', $Categories)->with('output',$output);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //


      /*  $this->validate($request, [
            'image_url'  => 'required|image|mimes:jpg,png,gif|max:2048'
            //   'upload_image'  => 'required|image|mimes:jpg,png,gif|max:2048'
        ]);*/
        $new_image_path="";
        if($request->file('image_url') != ''){
            $image = $request->file('image_url');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('Uploads/Products/'), $new_name);
            $new_image_path='Uploads/Products/'.$new_name;
        }

        if($new_image_path != '')
        {
            Products::where('id',$request->id)
                ->update([
                    'title' => $request->title,
                    'status' => $request->status,
                    'description' => $request->description,
                    'short_description' => $request->short_description,
                    'category_id' => $request->category_id,
                    'price' => $request->price,
                    'quantity' => $request->quantity,
                    'image_url' => $new_image_path
                ]);
        }else{
            Products::where('id',$request->id)
                ->update([
                    'title' => $request->title,
                    'status' => $request->status,
                    'description' => $request->description,
                    'short_description' => $request->short_description,
                    'category_id' => $request->category_id,
                    'price' => $request->price,
                    'quantity' => $request->quantity,
                ]);
        }



        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Products = Products::find($id);
        $Products->delete();

        // redirect
    //    Session::flash('message', 'Successfully deleted the Product!');
        return redirect('products');
    }




}
