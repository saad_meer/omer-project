<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use Illuminate\Http\Request;
use App\DataTables\Vendor\OrdersDataTable;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index1()
    {
        $orders = Orders::where('vendors_id', Auth::user()->id)->get();
        return view('vendor.orders')->with('orders', $orders);
    }

    public function index(Request $request, OrdersDataTable $dataTable)
    {
        if ($request->ajax()) {
            $query = Orders::where('orders.vendors_id', Auth::user()->id);
            return $dataTable->dataTable($query);
        }
        return view('vendor.orders');
    }

    public function getAll()
    {

        return view('admin.orders');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order=Orders::with('orderItems')->where('vendors_id', Auth::user()->id)->whereId($id)->first();
        if($order)
        {
            return view('vendor.orderdetail')->with('order',$order);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
