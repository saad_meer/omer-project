<?php

namespace App\Http\Traits;
use App\Models\Customers;  
use App\Models\Package;
use App\Models\PackageItem;
use App\Models\PackageShipping; 
use App\Models\MyClub; 
use Illuminate\Support\Facades\Auth;

trait PackageHelper
{
    public function createCustomer($customer,$package_schedule)
    {
        $get_customer=Customers::where('email',$customer['email'])->first();

        if(!$get_customer)
        {
            $stripe = new \Stripe\StripeClient(
                env('STRIPE_SECRET')
            );
            $strip_customer_id= $stripe->customers->create([
                'email' =>$customer['email'],
                'name' => $customer['name'],
                'payment_method'=>$package_schedule['stripe_payment_method']
            ]);

            $get_customer=new Customers();
            $get_customer->vendors_id=Auth::id();
            $get_customer->name=$customer['name'];
            $get_customer->email=$customer['email'];
            $get_customer->customers_id=$customer['id'];
            $get_customer->stripe_customer_id= $strip_customer_id['id'];
            $get_customer->save();
        }
        return $get_customer;
    }
    public function attachStripe($package_schedule,$get_customer)
    {
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
          );
          $stripe->paymentMethods->attach(
            $package_schedule['stripe_payment_method'],
            ['customer' =>  $get_customer->stripe_customer_id]
          );
    }
    public function packageStore($package_schedule,$get_customer)
    {
        $myclub=MyClub::where('id',$package_schedule['club_id'])->first();

        //save  package
        $package_schedule_save=new Package();
        $package_schedule_save->customers_id=$get_customer->id;
        $package_schedule_save->vendors_id=Auth::id();
        $package_schedule_save->orders_id=isset($package_schedule['orders_id'])?$package_schedule['orders_id']:NULL;
        $package_schedule_save->club_id = $myclub->id;
        $package_schedule_save->club_discount = $myclub->club_discount;
        $package_schedule_save->number_of_products = $myclub->number_of_products;
        $package_schedule_save->schedule_type=$package_schedule['schedule_type'];
        $package_schedule_save->schedule_value=$package_schedule['schedule_value'];
        $package_schedule_save->stripe_customer_id= $get_customer->stripe_customer_id;
        $package_schedule_save->stripe_payment_method=$package_schedule['stripe_payment_method'];
        $package_schedule_save->stripe_setup_intent_id=$package_schedule['stripe_setup_intent_id'];
        $package_schedule_save->save();
        return $package_schedule_save;
    }
    public function PackageShippingInfo($package_shipping_info,$package_schedule_save,$get_customer)
    {
        $package_shipping_info_save                  =  new PackageShipping();
        $package_shipping_info_save->customers_id    =  $get_customer->id;
        $package_shipping_info_save->vendors_id      =  Auth::id();
        $package_shipping_info_save->package_id      =  $package_schedule_save->id;
        $package_shipping_info_save->first_name      =  $package_shipping_info['first_name'];
        $package_shipping_info_save->last_name       =  $package_shipping_info['last_name'];
        $package_shipping_info_save->compnay         =  $package_shipping_info['company'];
        $package_shipping_info_save->email           =  $package_shipping_info['email'];
        $package_shipping_info_save->phone           =  $package_shipping_info['phone'];
        $package_shipping_info_save->country         =  $package_shipping_info['country'];
        $package_shipping_info_save->state           =  $package_shipping_info['state'];
        $package_shipping_info_save->city            =  $package_shipping_info['city'];
        $package_shipping_info_save->postcode        =  $package_shipping_info['postcode'];
        $package_shipping_info_save->address_line_1  =  $package_shipping_info['address_1'];
        $package_shipping_info_save->address_line_2  =  $package_shipping_info['address_2'];
        $package_shipping_info_save->save();
    }
    public function PackageSave($package_item,$package_schedule_save)
    {
        foreach($package_item as $package_product)
        {
            $package_item = new PackageItem();
            $package_item->package_id = $package_schedule_save->id;
            $package_item->product_id = $package_product['id'];
            $package_item->quantity = $package_product['quantity'];
            $package_item->title = $package_product['title'];
            $package_item->image_url = $package_product['image_url'];
            $package_item->price = $package_product['price'];
            $package_item->save();
        }
    }
}