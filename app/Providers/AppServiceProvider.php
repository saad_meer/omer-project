<?php

namespace App\Providers;

use App\Models\SubscriptionItems;
use App\Models\Subscriptions;
use App\Models\Vendors;
use Illuminate\Support\ServiceProvider;
use Laravel\Cashier\Cashier;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cashier::ignoreMigrations();
        Cashier::useCustomerModel(Vendors::class);
        Cashier::useSubscriptionModel(Subscriptions::class);
        Cashier::useSubscriptionItemModel(SubscriptionItems::class);
    }
}
