<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionItems extends Model
{
    use HasFactory;

    protected $table = 'vendor_subscription_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stripe_id',
        'stripe_product',
        'stripe_price',
        'quantity'
    ];
}
