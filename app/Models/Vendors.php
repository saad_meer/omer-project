<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Vendors extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Billable;

//    protected $table = 'vendors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'name',
        'email',
        'password',
        'title',
        'client_id',
        'access_token',
        'email',
        'web_url',
        'profile_image',
        'phone',
        'address',
        'about',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    /**
     * @var mixed
     */
    private $id;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function widget() {
        return $this->hasOne(Widgets::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscription() {
        return $this->hasOne(Subscriptions::class)->where('stripe_status','active')->orderByDesc('id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function customers()
    {
        return $this->hasMany(Customers::class, 'user_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function products()
    {
        return $this->hasMany(Products::class, 'user_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function orders()
    {
        return $this->hasMany(Orders::class, 'user_id');
    }
}
