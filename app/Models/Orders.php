<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendors_id',
        'customers_id',
        'orders_id',
        'shipping_tax',
        'shipping_total',
        'order_subtotal',
        'order_discount',
        'order_total',
        'billing_first_name',
        'billing_last_name',
        'billing_company',
        'billing_address_1',
        'billing_address_2',
        'billing_city',
        'billing_state',
        'billing_postcode',
        'billing_country',
        'billing_email',
        'billing_phone',
        'shipping_first_name',
        'shipping_last_name',
        'shipping_company',
        'shipping_address_1',
        'shipping_address_2',
        'shipping_city',
        'shipping_state',
        'shipping_postcode',
        'shipping_country',
        'shipping_method',
        'order_status',
        'order_data',
    ];
    /**
     * Get the vendor that owns the widget.
     */
    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendors_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function customer()
    {
        return $this->belongsTo(Customers::class, 'customers_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function orderItems()
    {
        return $this->hasMany(OrderItems::class, 'orders_id','id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function orderSchedules()
    {
        return $this->hasMany(OrderSchedules::class, 'orders_id');
    }
}
