<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderSchedules extends Model
{
    use HasFactory;

    /**
     * Get the vendor that owns the widget.
     */
    protected $table='order_schedules';

    protected $fillable = [
        'vendors_id',
        'orders_id',
        'schedule_type',
        'schedule_value',
    ];
    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendors_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function order()
    {
        return $this->belongsTo(Orders::class, 'orders_id');
    }
}
