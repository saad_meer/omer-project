<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageShipping extends Model
{
    use HasFactory; 
    protected $table='package_shipping_info';

    protected $fillable = [
       'customers_id',
       'vendors_id',
       'package_id',
       'first_name',
       'last_name',
       'compnay',
       'email',
       'phone',
       'country',
       'state',
       'city',
       'postcode',
       'address_line_1',
       'address_line_2',
    ];
}
