<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    use HasFactory;

    /**
     * Get the vendor that owns the widget.
     */
    protected $fillable = [
        'vendors_id',
        'orders_id',
        'products_id',
        'variation_id',
        'product_name',
        'total',
        'product_type',
    ];
    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendors_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function product()
    {
        return $this->hasOne(Products::class, 'products_id');
    }
}
