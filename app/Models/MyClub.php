<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MyClub extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='myclub';

    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendors_id');
    }
    public function package()
    {
        return $this->hasMany(Package::class, 'club_id','id');
    }
    protected $fillable = [
        'vendors_id',
        'club_title',
        'number_of_products',
        'club_discount',
        'club_image_url',
    ];
}
