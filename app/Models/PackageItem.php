<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageItem extends Model
{
    use HasFactory;
    protected $table='packages_items';

    protected $fillable = [
       'package_id',
       'product_id',
       'woocomerce_product_id',
       'title',
       'image_url',
       'price',
    ];
}
