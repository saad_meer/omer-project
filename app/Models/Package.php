<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $table='packages';

    protected $fillable = [
        'customers_id',
       'vendors_id',
       'orders_id',
       'club_id',
       'club_discount',
       'number_of_products',
        'schedule_type',
        'schedule_value',
        'period',
        'period_datetime',
        'stripe_customer_id',
        'stripe_payment_method',
        'stripe_setup_intent_id',
        'stripe_card_expiry',
        'status',
    ];

    public function myclub()
    {
        return $this->belongsTo(MyClub::class, 'club_id');
    }
    public function packageItem()
    {
        return $this->hasMany(PackageItem::class, 'package_id');
    }
}
