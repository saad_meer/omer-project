<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Subscription as CashierSubscription;

class Subscriptions extends CashierSubscription
{
    use HasFactory;

    protected $table = 'vendor_subscriptions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level',
        'name',
        'stripe_id',
        'stripe_status',
        'stripe_price',
        'quantity',
        'stripe_plan',
        'trial_ends',
        'ends_at',
        'status',
        'period',
        'paid_by',
    ];

    /**
     * Get the vendor that owns the widget.
     */
    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendors_id');
    }

    public static function disableAll($vendor_id) {
        $affected = DB::table('vendor_subscriptions')
            ->where('vendors_id', $vendor_id)
            ->update(['status' => 0]);

        return $affected;
    }

}
