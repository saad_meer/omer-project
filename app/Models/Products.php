<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    /**
     * Get the vendor that owns the widget.
     */

    protected $fillable = [
        'product_id',
        'vendor_id',
        'category_id',
        'title',
        'slug',
        'status',
        'is_feature',
        'description',
        'short_description',
        'price',
        'sale_price',
        'quantity',
        'weight',
        'length',
        'width',
        'height',
        'products',
        'data',
    ];
    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendor_id');
    }
    public static  function get_product_id($id)
    {
        $product=Products::where('product_id',$id)->first('id');
        if($product)
        {
            return $product->id;
        }
        return NULL;
    }
}
