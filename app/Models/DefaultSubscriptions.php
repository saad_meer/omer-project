<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DefaultSubscriptions extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level',
        'price',
        'period',
        'features',
        'description',
    ];

    public static function getExpiry($period) {
        $expiry = date('Y-m-d', strtotime('+1 month'));
        if ($period == SUBSCRIPTION_EXPIRY_YEARLY) {
            $expiry = date('Y-m-d', strtotime('+1 year'));
        }

        return $expiry;
    }
}
