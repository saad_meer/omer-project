<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Widgets extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'width',
        'height',
        'content',
    ];
    /**
     * @var mixed
     */
    private $vendor_id;

    /**
     * Get the vendor that owns the widget.
     */
    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendors_id');
    }
}
