<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendors_id',
        'customers_id',
        'name',
        'email',
        'phone',
        'birthday',
        'billing_address',
        'shipping_address',
        'is_guest',
        'stripe_customer_id',
        'stripe_payment_method_id',
        'stripe_setup_intent_id',
    ];
    /**
     * Get the vendor that owns the widget.
     */
    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendors_id');
    }

    /**
     * Get the vendor that owns the widget.
     */
    public function orders()
    {
        return $this->hasMany(Orders::class, 'orders_id');
    }
}
