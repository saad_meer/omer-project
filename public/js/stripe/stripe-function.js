const stripe = Stripe('pk_test_51JMN9oJumPH44BW49PgGmg6r0q7fjOEd26o1RQj5EJMdGE38E9rVvs0x6auuYn6ntCQf9tHepTqaxsBDY2LQJ97500oX5C2flR');
        const elements = stripe.elements();

        var style = {
            base: {
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
        var cardElement = elements.create('card', {hidePostalCode: true, style: style});

        cardElement.mount('#moi-card-element');

        const cardHolderName = document.getElementById('moi-card-holder-name');
        const cardButton = document.getElementById('moi-card-button');
        const clientSecret = cardButton.getAttribute('data-secret');

        cardButton.addEventListener('click', async (e) => {
            const { setupIntent, error } = await stripe.confirmCardSetup(
                cardButton.getAttribute('data-secret'), {
                    payment_method: {
                        card: cardElement,
                        billing_details: { name: document.getElementById('moi-card-holder-name').value }
                    }
                }
            );

            if (error) {
                jQuery('.moi-card-errors').html(error['message']);
                console.log(error['message'])
            } else {
                jQuery('.moi-card-errors').html('');
                var model_call = document.getElementById("moi-card-button").getAttribute("data-model-click");
                moi_call_back_model_function(setupIntent.id,setupIntent.payment_method,model_call);
            }
        });
