<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::impersonate();

Route::get('/', function () {
//    return view('welcome');
    return view('coming-soon');
});

Route::get('/test', function () {
//    return view('welcome');
    return view('productlisting');
});




Route::get('cart', [\App\Http\Controllers\AdminDashboardController::class, 'cart'])->name('cart');

Route::get('add-to-cart/{id}', [\App\Http\Controllers\AdminDashboardController::class, 'addToCart'])->name('add.to.cart');

Route::Post('add-to-cart', [\App\Http\Controllers\AdminDashboardController::class, 'addToCartAjax'])->name('add.to.carts');

Route::patch('update-cart', [\App\Http\Controllers\AdminDashboardController::class, 'updateCart'])->name('update.cart');

Route::delete('remove-from-cart', [\App\Http\Controllers\AdminDashboardController::class, 'removeCart'])->name('remove.from.cart');

Route::get('checkout', [\App\Http\Controllers\AdminDashboardController::class, 'checkout']);
Route::post('create_order', [\App\Http\Controllers\ClubController::class, 'createOrder']);


Route::get('INSTAKTER', [\App\Http\Controllers\AdminDashboardController::class, 'productsVendor'])->name('productsVendor');

Route::get('INSTAKTER/{id}', [\App\Http\Controllers\AdminDashboardController::class, 'vendorLists'])->name('vendorLists');

Route::get('INSTAKTER/{id}/{categoryid}',[\App\Http\Controllers\AdminDashboardController::class, 'vendorLists2'])->name('vendorLists2');

Route::POST('productData', [\App\Http\Controllers\AdminDashboardController::class, 'productData'])->name('productData');
 
//Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//    return view('dashboard');
//})->name('dashboard');

//Route::middleware(['auth:sanctum', 'verified'])->group(['prefix' => 'admin'], function () {
//    Route::get('/', [\App\Http\Controllers\AdminDashboardController::class, 'index'])->name('admin');
//});

    Route::group(['middleware' => 'auth:admin', 'prefix'=> 'admin', 'as' => 'admin.'], function() {
        Route::get('dashboard', [\App\Http\Controllers\AdminDashboardController::class, 'index'])->name('dashboard');
        Route::post('loginAsVendor', [\App\Http\Controllers\AdminDashboardController::class, 'loginAsVendor'])->name('loginAsVendor');

        Route::get('vendorsList', [\App\Http\Controllers\AdminDashboardController::class, 'vendorsList'])->name('vendorsList');

        Route::get('customersList', [\App\Http\Controllers\AdminDashboardController::class, 'customersList'])->name('customersList');


        Route::get('ordersList', [\App\Http\Controllers\AdminDashboardController::class, 'ordersList'])->name('ordersList');


        //categories
        Route::get('categories', [\App\Http\Controllers\CategoryController::class, 'categoryList'])->name('categories');
        Route::get('admin/removeCategory', [\App\Http\Controllers\CategoryController::class, 'removeCategory'])->name('removeCategory');
        Route::get('admin/editCategory', [\App\Http\Controllers\CategoryController::class, 'showCategory'])->name('editCategory');
        Route::post('Category', [\App\Http\Controllers\CategoryController::class, 'storeCategory'])->name('Category');



        //   Route::get('productsList/{id?}', [\App\Http\Controllers\AdminDashboardController::class, 'productsList'])->name('productsList');
        Route::get('productsList', [\App\Http\Controllers\AdminDashboardController::class, 'productsList'])->name('productsList');
        Route::get('productsCreate', [\App\Http\Controllers\AdminDashboardController::class, 'productsCreate'])->name('productsCreate');
        Route::post('productADD', [\App\Http\Controllers\AdminDashboardController::class, 'productADD'])->name('productADD');
        Route::get('productView/{id}', [\App\Http\Controllers\AdminDashboardController::class, 'productView'])->name('productView');
        Route::post('productUpdate', [\App\Http\Controllers\AdminDashboardController::class, 'productUpdate'])->name('productUpdate');
        Route::post('productDelete', [\App\Http\Controllers\AdminDashboardController::class, 'productDelete'])->name('productDelete');

        Route::get('delProducts', [\App\Http\Controllers\AdminDashboardController::class, 'Removeproducts'])->name('DelProducts');

        Route::get('CategoryList', [\App\Http\Controllers\AdminDashboardController::class, 'categoryList'])->name('CategoryList');
        Route::get('removeCategory', [\App\Http\Controllers\AdminDashboardController::class, 'removeCategory'])->name('removeCategory');
        Route::get('editCategory', [\App\Http\Controllers\AdminDashboardController::class, 'showCategory'])->name('editCategory');
        Route::post('AddCategory', [\App\Http\Controllers\AdminDashboardController::class, 'storeCategory'])->name('AddCategory');

        Route::get('AdminProfile', [\App\Http\Controllers\AdminDashboardController::class, 'adminProfile'])->name('AdminProfile');
        Route::post('AdminProfileUpdate', [\App\Http\Controllers\AdminDashboardController::class, 'adminProfileUpdate'])->name('AdminProfileUpdate');

      //  Route::get('viewWidgets/{id?}', [\App\Http\Controllers\AdminDashboardController::class, 'viewWidgets'])->name('viewWidgets');
        Route::get('viewWidgets', [\App\Http\Controllers\AdminDashboardController::class, 'viewWidgets'])->name('viewWidgets');

     //   Route::get('viewMemberships/{id?}', [\App\Http\Controllers\AdminDashboardController::class, 'viewMemberships'])->name('viewMemberships');
        Route::get('viewMemberships', [\App\Http\Controllers\AdminDashboardController::class, 'viewMemberships'])->name('viewMemberships');

        Route::resource('defaultWidget', \App\Http\Controllers\DefaultWidgetController::class);

        Route::resource('defaultMemberships', \App\Http\Controllers\DefaultSubscriptionController::class);


    });



    Route::group(['middleware' => ['auth:vendor']], function() {

        Route::get('vendor/dashboard', [App\Http\Controllers\VendorDashboardController::class , 'index'])->name('vendor.dashboard');
        Route::resource('customers', \App\Http\Controllers\CustomersController::class);

        Route::resource('orders', \App\Http\Controllers\OrdersController::class);
        Route::resource('widget', \App\Http\Controllers\WidgetController::class);
        Route::resource('membership', \App\Http\Controllers\SubscriptionController::class);
        Route::resource('vendor', \App\Http\Controllers\VendorsController::class);
        Route::resource('products', \App\Http\Controllers\ProductsController::class);
        Route::get('updateSettings', [\App\Http\Controllers\VendorsController::class, 'updateSettings'])->name('updateSettings');
        Route::post('updateVendorSettings', [\App\Http\Controllers\VendorsController::class, 'updateVendorSettings'])->name('updateVendorSettings');

        Route::get('pluginSetting', [\App\Http\Controllers\VendorsController::class, 'pluginSetting']);
        Route::get('updateAccessToken/{id}', [\App\Http\Controllers\VendorsController::class, 'updateAccessToken']);
        Route::resource('myclub', \App\Http\Controllers\ClubController::class);

        Route::get('clubDetails', [\App\Http\Controllers\ClubController::class, 'index'])->name('ClubListing');



        //products vendor
        Route::resource('products', \App\Http\Controllers\ProductsController::class);
        Route::post('products/{product}', [\App\Http\Controllers\ProductsController::class, 'update'])->name('products.update');


        Route::get('customerDetails', [\App\Http\Controllers\CustomersController::class, 'index'])->name('CustomerListing');
        Route::get('customers/{id}', [\App\Http\Controllers\CustomersController::class, 'show'])->name('customers.view');

        Route::get('orderDetails', [\App\Http\Controllers\OrdersController::class, 'index'])->name('OrderListing');
        Route::get('orders/{id}', [\App\Http\Controllers\OrdersController::class, 'show'])->name('orders.view');




        Route::get('myclub/delete/{id}',[ \App\Http\Controllers\ClubController::class,'destroy']);
    });



Route::get('/admin/login', [\App\Http\Controllers\Auth\LoginController::class, 'showAdminLoginForm'])->name('admin/login');
Route::post('/admin/login', [\App\Http\Controllers\Auth\LoginController::class, 'userLogin'])->name('admin/login');
Route::post('/admin/logout', [\App\Http\Controllers\Auth\LogoutController::class, 'userLogout'])->name('admin/logout');


Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class,'showVendorLogin'])->name('login');
Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class,'vendorLogin'])->name('login');

Route::post('/registervendor', [\App\Http\Controllers\Auth\RegisterController::class,'registerVendor'])->name('registervendor');

Route::get('logout',[App\Http\Controllers\Auth\LogoutController::class , 'userLogout'])->name('logout');
