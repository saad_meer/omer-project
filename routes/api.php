<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/validateVendor', [\App\Http\Controllers\Api\VendorController::class, 'validateVendor']);

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/add_customer', [\App\Http\Controllers\Api\CustomerController::class, 'store']);

    Route::post('/check_register_customer', [\App\Http\Controllers\Api\CustomerController::class, 'checkRegisterCustomer']);
    Route::post('/get_customer_address', [\App\Http\Controllers\Api\CustomerController::class, 'get_customer_address']);
    Route::post('/get_customer_packages', [\App\Http\Controllers\Api\CustomerController::class, 'get_customer_packages']);

    Route::get('/get_register_product', [\App\Http\Controllers\Api\ProductController::class, 'index']);
    Route::post('/register_product', [\App\Http\Controllers\Api\ProductController::class, 'store']);

    Route::post('/place_order', [\App\Http\Controllers\Api\OrderController::class, 'store']);
    Route::post('/get_customer_orders', [\App\Http\Controllers\Api\OrderController::class, 'get_customer_orders']);

    Route::post('/save_schedule', [\App\Http\Controllers\Api\PackageController::class, 'saveSchedule']);

    Route::get('/get_vendors_clubs', [\App\Http\Controllers\Api\OrderController::class, 'get_vendors_clubs']);

});
