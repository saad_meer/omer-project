<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('vendors_id')->unsigned()->index();
            $table->foreign('vendors_id')
                ->references('id')
                ->on('vendors')
                ->onDelete('cascade');
            $table->enum('level', [SUBSCRIPTION_LEVEL_BASIC, SUBSCRIPTION_LEVEL_STANDARD, SUBSCRIPTION_LEVEL_PREMIUM]);
            $table->string('name');
            $table->string('stripe_id');
            $table->string('stripe_status');
            $table->string('stripe_price')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('stripe_plan')->nullable();
            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamp('ends_at')->nullable();

            $table->boolean('status')->default(1);
            $table->enum('period', [SUBSCRIPTION_EXPIRY_MONTHLY, SUBSCRIPTION_EXPIRY_YEARLY]);
            $table->string('paid_by')->nullable();
            $table->timestamps();


            $table->index(['vendors_id', 'stripe_status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_subscriptions');
    }
}
