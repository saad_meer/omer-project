<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefaultSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->enum('level',[SUBSCRIPTION_LEVEL_BASIC, SUBSCRIPTION_LEVEL_STANDARD, SUBSCRIPTION_LEVEL_PREMIUM]);
            $table->decimal('price');
            $table->enum('period', [SUBSCRIPTION_EXPIRY_MONTHLY, SUBSCRIPTION_EXPIRY_YEARLY]);
            $table->text('features')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_subscriptions');
    }
}
