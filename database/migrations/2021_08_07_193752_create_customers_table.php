<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('vendors_id')->unsigned()->index();
            $table->bigInteger('customers_id')->unsigned()->index();
            $table->foreign('vendors_id')
                ->references('id')
                ->on('vendors')
                ->onDelete('cascade');
            $table->text('stripe_customer_id')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->date('birthday')->nullable();
            $table->string('language')->default('EN');
            $table->string('billing_address')->nullable();
            $table->string('shipping_address')->nullable();
            $table->boolean('payment_status')->default(0);
            $table->dateTime('payment_done_at')->nullable();
            $table->boolean('newsletter')->default(0);
            $table->boolean('is_guest')->default(1);
            $table->boolean('status')->default(1);
            $table->json('custom_fields')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
