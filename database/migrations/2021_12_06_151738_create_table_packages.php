<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->integer('customers_id')->unsigned();
            $table->bigInteger('vendors_id')->unsigned()->index();
            $table->bigInteger('orders_id')->nullable();
            $table->integer('club_id')->unsigned()->index();
            $table->integer('number_of_products')->nullable();
            $table->integer('club_discount')->nullable();
            $table->string('schedule_type')->nullable();
            $table->string('schedule_value')->nullable();
            $table->string('period')->nullable();
            $table->string('period_datetime')->nullable();
            $table->text('stripe_customer_id')->nullable();
            $table->text('stripe_payment_method')->nullable();
            $table->text('stripe_setup_intent_id')->nullable();
            $table->text('stripe_card_expiry')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
