<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyclub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('myclub', function (Blueprint $table) {
            $table->id();
            $table->integer('vendors_id');
            $table->string('club_title')->nullable();
            $table->integer('number_of_products')->nullable();
            $table->integer('club_discount')->nullable();
            $table->string('club_image_url')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('myclub');
    }
}
