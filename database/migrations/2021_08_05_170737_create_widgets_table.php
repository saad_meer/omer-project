<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('vendors_id')->unsigned()->index();
            $table->foreign('vendors_id')
                ->references('id')
                ->on('vendors')
                ->onDelete('cascade');
            $table->boolean('status')->default(1);
            $table->string('title');
            $table->string('description');
            $table->string('width');
            $table->string('height');
            $table->string('token', 64)->unique();
            $table->longText('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widgets');
    }
}
