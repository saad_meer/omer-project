<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorSubscriptionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_subscription_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subscriptions_id')->unsigned()->index();
            $table->foreign('subscriptions_id')
                ->references('id')
                ->on('vendor_subscriptions')
                ->onDelete('cascade');
            $table->string('stripe_id')->index();
            $table->string('stripe_product');
            $table->string('stripe_price');
            $table->integer('quantity')->nullable();
            $table->timestamps();

            $table->unique(['subscriptions_id', 'stripe_price']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_subscription_items');
    }
}
