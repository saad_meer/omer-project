<?php

namespace Database\Seeders;

use App\Models\DefaultWidget;
use Illuminate\Database\Seeder;

class DefaultWidgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $widget =  new DefaultWidget();
        $widget->title = 'You can please schedule this order!';
        $widget->description = 'It will be delivered to you on your scheduled datetime.';
        $widget->width = '400px';
        $widget->height = '300px';
        $widget->content = '<h1>widget content here</h1>';
        $widget->save();
    }
}
