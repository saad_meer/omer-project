<?php

namespace Database\Seeders;

use App\Models\DefaultSubscriptions;
use App\Models\DefaultWidget;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DefaultWidgetSeeder::class,
            DefaultSubscriptionsSeeder::class,
            UserSeeder::class,
            RolesSeeder::class
        ]);
    }
}
