<?php

namespace Database\Seeders;

use App\Models\DefaultSubscriptions;
use Illuminate\Database\Seeder;

class DefaultSubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscription =  new DefaultSubscriptions();
        $subscription->level = SUBSCRIPTION_LEVEL_BASIC;
        $subscription->price = 100;
        $subscription->period = SUBSCRIPTION_EXPIRY_MONTHLY;
        $subscription->features = 'Support upto 10 products';
        $subscription->description = 'Basic Plan to join Members-One.';
        $subscription->save();

        $subscription =  new DefaultSubscriptions();
        $subscription->level = SUBSCRIPTION_LEVEL_STANDARD;
        $subscription->price = 200;
        $subscription->period = SUBSCRIPTION_EXPIRY_MONTHLY;
        $subscription->features = 'Support upto 50 products';
        $subscription->description = 'Standard Plan to join Members-One.';
        $subscription->save();

        $subscription =  new DefaultSubscriptions();
        $subscription->level = SUBSCRIPTION_LEVEL_PREMIUM;
        $subscription->price = 300;
        $subscription->period = SUBSCRIPTION_EXPIRY_MONTHLY;
        $subscription->features = 'Support upto 100 products,Offer the Promotions & Discounts to customers';
        $subscription->description = 'Premium Plan to join Members-One.';
        $subscription->save();
    }
}
