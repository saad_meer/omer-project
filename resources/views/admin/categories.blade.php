@extends('layouts.admin')
@section('title', 'Dashboard')

@section('head')
    @parent



    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('page-title', 'My Clubs')
@section('content')
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-6">
                        <h4>Categories List</h4>

                    </div>

                    <div class="col-xl-6 col-md-6 col-sm-6 col-6" style="text-align: right;margin-top: 10px;">
                        <button type="button" class="btn btn-outline-primary" id="addCategory">Add Category</button>
                        <!--                        { { url('myclub/create')}}-->
                    </div>


                </div>
                @if(session()->has('message'))
                    <div class="alert alert-success mb-4" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round"
                                 class="feather feather-x close" data-dismiss="alert">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                        {{ session()->get('message') }}</button>
                    </div>
                @endif

            </div>
        </div>
        <div class="widget-content widget-content-area">
            <div class="table-responsive mb-4">
                <div class="table-responsive mb-4">
                    <table id="vendor_categories_dt" class="table table-bordered"
                           style="width:100%;">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Category Title</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



    <div id="categoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="category_form">
                    <div class="modal-header">
                        <h4 class="modal-title text-left" style="margin-left:0;">Add Category</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                        {{csrf_field()}}
                        <span id="form_output"></span>
                        <div class="form-group">
                            <label>Category Tilte</label>
                            <input type="text" name="category_title" id="category_title" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Category Slug</label>
                            <input type="text" name="category_slug" id="category_slug" class="form-control"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="category_id" id="category_id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="insert"/>
                        <input type="submit" name="submit" id="action" value="Add" class="btn btn-info"/>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection



@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <script src="{{asset('vendor-dashboard/plugins/sweetalerts/promise-polyfill.js')}}"></script>
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>

    <!-- END PAGE LEVEL SCRIPTS -->

    <script src="{{asset('vendor-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var table = $('#vendor_categories_dt').DataTable({

                //orderCellsTop: true,
                fixedHeader: true,
                processing: true,
                serverSide: true,
                ajax: "{{route('admin.categories')}}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'categoryTitle', name: 'categoryTitle'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {"targets": 0, "width": "70px", "className": "ta-c"},
                    {"targets": 1, "width": "120px", "className": "ta-c"},
                    {"targets": 2, "width": "100px", "className": "ta-c"}
                ]
            });

            // $('.filter-select').change(function () {
            //     table.columns($(this).data('column'))
            //         .search($(this).val())
            //         .draw();
            // });


        });

        $('#addCategory').click(function () {
            $('#categoryModal').modal('show');
            $('#category_form')[0].reset();
            $('#form_output').html('');
            $('#button_action').val('insert');
            $('#action').val('Add');
        });

        $('#category_form').on('submit', function (event) {
            event.preventDefault();
            var form_data = $(this).serialize();
            $.ajax({
                url: "{{ route('admin.Category') }}",
                method: "POST",
                data: form_data,
                dataType: "json",
                success: function (data) {
                    if (data.error.length > 0) {
                        var error_html = '';
                        for (var count = 0; count < data.error.length; count++) {
                            error_html += '<div class="alert alert-danger">' + data.error[count] + '</div>';
                        }
                        $('#form_output').html(error_html);
                    } else {
                        $('#form_output').html(data.success);


                        $('#category_form')[0].reset();
                        $('#action').val('Add');
                        $('.modal-title').text('Add Data');
                        $('#button_action').val('insert');
                        $('#vendor_categories_dt').DataTable().ajax.reload();

                        // $('#categoryModal').modal('hide');
                        $('.btn-default').click();
                        toastr.success('Category added Succesfully', {timeOut: 5000});
                    }
                }
            })
        });


        $(document).on('click', '.edit', function () {
            var id = $(this).attr("id");
            $('#form_output').html('');
            $.ajax({
                url: "{{route('admin.editCategory')}}",
                method: 'get',
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    $('#category_title').val(data.categoryTitle);
                    $('#category_slug').val(data.categorySlug);
                    console.log(id);
                    $('#category_id').val(id);
                    $('#categoryModal').modal('show');
                    $('#action').val('Edit');
                    $('.modal-title').text('Edit Data');
                    $('#button_action').val('update');
                }
            })
        });

        $(document).on('click', '.delete', function () {
            var id = $(this).attr('id');


            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                padding: '1em'
            }).then(function (result) {
                if (result.value)
                {

                    $.ajax({
                        url: "{{route('admin.removeCategory')}}",
                        mehtod: "get",
                        data: {id: id},
                        success: function (data) {
                            // alert(data);
                            swal(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            //$('#club-table').DataTable().ajax.reload();
                            toastr.success('Category deleted Succesfully', {timeOut: 5000});
                            $('#vendor_categories_dt').DataTable().ajax.reload();
                        }
                    });
                }
            });

        });


    </script>

@endsection
<!-- BEGIN PAGE LEVEL SCRIPTS -->

