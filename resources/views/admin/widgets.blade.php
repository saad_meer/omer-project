
@extends('layouts.admin')
@section('title', 'Dashboard - Widgets List')
@section('head')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('admin-dashboard/plugins/table/datatable/datatables.css') }}">
<link rel="stylesheet" type="text/css"
     href="{{ asset('admin-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
<link rel="stylesheet" type="text/css"
     href="{{ asset('admin-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
<link rel="stylesheet" type="text/css"
     href="{{ asset('admin-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('content')
<div class="col-lg-12">
   <div class="statbox widget box box-shadow">
       <div class="widget-header">
           <div class="row">
           <div class="col-xl-12 col-md-12 col-sm-12 col-12">
               <h4>Widgets List</h4>
           </div>
           </div>
           <div class=" d-flex justify-content-center">
               <div col-md-3>
                   <select class="selectpicker filter-select" data-column="1">
                       <option>All Vendors</option>
                       @foreach($vendors as $vendor)
                           <option value="{{$vendor->title}}">{{$vendor->title}}</option>
                       @endforeach
                   </select>
               </div>
           </div>
       </div>
       <div class="widget-content widget-content-area">
           <div class="table-responsive mb-4">
               <table id="admin_widgets_dt"
                      class="table table-bordered"
                      style="width:100%;">
                   <thead>
                   <tr>
                       <th>ID</th>
                       <th>Title</th>
                       <th>Description</th>
                       <th>Width</th>
                       <th>Height</th>
                       <th>Token</th>
                       <th>Vendor</th>
                       <th>Status</th>
                       <th>Created</th>
                       <th>Updated</th>
                       <th class="text-center">Action</th>
                   </tr>
                   </thead>
                   <tbody></tbody>
               </table>
           </div>
       </div>
   </div>
</div>
@endsection

@section('footer')
@parent
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('admin-dashboard/plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('admin-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    $(document).ready(function () {

        var table = $('#admin_widgets_dt').DataTable({

            //orderCellsTop: true,
            fixedHeader: true,
            processing: true,
            serverSide: true,
            ajax: "{{route('admin.viewWidgets')}}",
            columns: [
                {data: 'widgets_id', name: 'widgets.id'},
                {data: 'widgets_title', name: 'widgets.title'},

                {data: 'description', name: 'description'},
                {data: 'width', name: 'width'},
                {data: 'height', name: 'height'},
                {data: 'token', name: 'token'},
                {data: 'vendor_company', name: 'vendors.title'},
                {data: 'widgets_status', name: 'widgets.status'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            columnDefs: [
                {"targets": 0, "width": "100px", "className": "ta-c"},
                {"targets": 5, "width": "140px", "className": "ta-c"}
            ]
        });
        $('.filter-select').change(function () {
            table.columns($(this).data('column'))
                .search($(this).val())
                .draw();
        });

    });


</script>

@endsection
