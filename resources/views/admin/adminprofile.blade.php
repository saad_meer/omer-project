@extends('layouts.admin')
@section('title', 'Admin Profile')
@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('content')
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Admin Profile</h4>
                    </div>
                </div>

            </div>
            <div class="widget-content widget-content-area">
                <div class=" mb-4">
                    <form method="post" action="{{route('admin.AdminProfileUpdate')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label>User Name</label>
                                <input type="text" name="name" class="form-control" value="{{ $admin->name ?? '' }}" id="name" placeholder="Full Name">


                                <label>User Email</label>
                                                              <input type="email" name="email" value="{{$admin->email ?? '' }}" class="form-control" id="email"
                                                                     placeholder="Email address">

<!--                                <label>User Password</label>
                                <input type="password" name="password" value="{ {$admin->password  ?? '' }}" class="form-control" id="password"
                                       placeholder="Password *">-->

                                <input type="submit" class="btn btn-lg btn-primary mt-3" value="Submit" />
                            </div>
                            <div class="col-md-6" >
                                <div class="ml-3 custom-file-container" data-upload-id="myFirstImage">
                                    <label>Upload (Single File) <a href="javascript:void(0)"
                                                                   class="custom-file-container__image-clear"
                                                                   title="Clear Image">x</a></label>
                                    <label class="custom-file-container__custom-file">

                                        <input type="file" name="upload_image" class="custom-file-container__custom-file__custom-file-input"   accept="image/*" value="" >
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                        <span class="custom-file-container__custom-file__custom-file-control"></span>
                                    </label>
                                    @if($admin->profile_photo_path != '')
                                        <style>
                                            .custom-file-container__image-preview {
                                                ox-sizing: border-box;
                                                transition: all 0.2s ease;
                                                margin-top: 54px;
                                                margin-bottom: 40px;
                                                height: 250px;
                                                width: 100%;
                                                border-radius: 4px;
                                                background-size: contain;
                                                background-position: center center;
                                                background-repeat: no-repeat;
                                                background-color: #fff;
                                                overflow: auto;
                                                padding: 15px;
                                            }
                                        </style>
<!--                                    div m backgroud image lagae gy tu ok ho jae gaa kaam   style='background-image: url("{ {asset($admin->profile_photo_path ?? '')}}")' -->

                                        <img class="custom-file-container__image-preview" style="  margin-top: 54px;
                                        overflow: auto;
                                                padding: 15px;
                                                margin-bottom: 40px;
                                                height: 250px;
                                                width: 100%;
                                                border-radius: 4px;" src="{{asset($admin->profile_photo_path ?? '')}}">
                                    @else
                                        <div class="custom-file-container__image-preview" ></div>
                                    @endif


                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('admin-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('admin-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
    </script>
@endsection
