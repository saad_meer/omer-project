<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-dashboard/assets/js/loader.js') }}"></script>

<script src="{{ asset('admin-dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('admin-dashboard/assets/js/app.js') }}"></script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="{{ asset('admin-dashboard/assets/js/custom.js') }}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{ asset('admin-dashboard/plugins/apex/apexcharts.min.js') }}"></script>
<script src="{{ asset('admin-dashboard/assets/js/dashboard/dash_2.js') }}"></script>

<script src="{{ asset('toastr/toastr.min.js') }}"></script>
<script src="{{asset('vendor-dashboard/plugins/sweetalerts/sweetalert2.min.js')}}"></script>
<script src="{{asset('vendor-dashboard/plugins/sweetalerts/custom-sweetalert.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/js/file-upload-with-preview.min.js')}}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/js/highlight.pack.js')}}"></script>
<script src="{{asset('assets/js/scrollspyNav.js')}}"></script>
<script src="{{asset('assets/js/jquery.steps.min.js')}}"></script>
<!--<script src="{ {asset('assets/js/custom-jquery.steps.js')}}"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->        <!-- BEGIN PAGE LEVEL SCRIPTS -->


