@extends('layouts.admin')
@section('title', 'Dashboard')
@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('content')
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Products List</h4>
                    </div>
                </div>


                    <div class=" d-flex justify-content-center">
                        <div col-md-3>
                            <select class="selectpicker filter-select" data-column="1">
                                <option value="all">All Vendors</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{$vendor->title}}" >{{$vendor->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                    <table id="admin_productss_dt"
                           class="table table-bordered"
                           style="width:100%;">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Vendor</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Stock</th>
                            <th>Image</th>
                            <th>Status</th>
<!--                            <th class="text-center">Action</th>-->
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('admin-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('admin-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function () {

            var table = $('#admin_productss_dt').DataTable({

                //orderCellsTop: true,
                fixedHeader: true,
                processing: true,
                serverSide: true,
                ajax: "{{route('admin.productsList')}}",
                columns: [
                    {data: 'products_id', name: 'products.id'},
                    {data: 'vendor_company', name: 'vendors.title'},
                    {data: 'products_title', name: 'products.title'},
                    {data: 'price', name: 'price'},
                    {data: 'quantity', name: 'quantity'},
                    {data: 'image_url', name: 'image_url'},
                    {data: 'products_status', name: 'products.status'},/*
                    {data: 'action', name: 'action', orderable: false, searchable: false},*/
                ],
                columnDefs: [
                    {"targets": 0, "width": "100px", "className": "ta-c"},
                    {"targets": 5, "width": "140px", "className": "ta-c"}
                ]
            });
            $('.filter-select').change(function () {
                if ($(this).val() !== "all") {
                     table.columns($(this).data('column'))
                         .search($(this).val())
                         .draw();
                 } else {
                     table.columns($(this).data('column'))
                         .search("")
                         .draw();
                 }
            });



        });




    </script>
@endsection
