@extends('layouts.admin')
@section('title', 'Dashboard')

@section('head')
    @parent
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin-dashboard/assets/css/forms/switches.css')}}">
    <link href="{{ asset('admin-dashboard/plugins/pricing-table/css/component.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
@endsection
@section('content')
    <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Subscription Settings</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="container">
                    <section class="pricing-section bg-7 mt-5">
                        <div class="pricing pricing--norbu">
                            <div class="pricing__item">
                                <form method="POST" action="{{route('admin.defaultSubscription.update', $basic->id)}}">
                                    @method('PUT')
                                    @csrf
                                    <input type="hidden" name="level" value="{{$basic->level}}">
                                    <h3 class="pricing__title">{{ \Illuminate\Support\Str::upper($basic->level) }}</h3>
                                    <p class="pricing__sentence">Level Description here...</p>
                                    <div class="pricing__price"><span class="pricing__currency">$</span><input type="text" name="price" value="{{$basic->price}}" class="default-subscription-input"><span class="pricing__period">/ {{ ($basic->period==SUBSCRIPTION_EXPIRY_MONTHLY)? 'month':'year' }}</span></div>

                                    <ul class="pricing__feature-list text-center">
                                        @foreach(explode(',', $basic->features) as $feature)
                                            <li class="pricing__feature"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg> {{$feature}}</li>
                                        @endforeach
                                    </ul>
                                    <button type="submit" class="pricing__action mx-auto mb-4 btn-primary">Save Changes!</button>
                                </form>
                            </div>

                            <div class="pricing__item pricing__item--featured">
                                <form method="POST" action="{{route('admin.defaultSubscription.update', $standard->id)}}">
                                    @method('PUT')
                                    @csrf
                                    <input type="hidden" name="level" value="{{$standard->level}}">
                                    <h3 class="pricing__title">{{ \Illuminate\Support\Str::upper($standard->level) }}</h3>
                                    <p class="pricing__sentence">Level Description here...</p>
                                    <div class="pricing__price"><span class="pricing__currency">$</span><input type="text" name="price" value="{{$standard->price}}" class="default-subscription-input"><span class="pricing__period">/ {{ ($standard->period==SUBSCRIPTION_EXPIRY_MONTHLY)? 'month':'year' }}</span></div>
                                    <ul class="pricing__feature-list text-center">
                                        @foreach(explode(',', $standard->features) as $feature)
                                            <li class="pricing__feature"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg> {{$feature}}</li>
                                        @endforeach
                                    </ul>
                                    <button type="submit" class="pricing__action mx-auto mb-4 btn-primary">Save Changes!</button>
                                </form>
                            </div>

                            <div class="pricing__item">
                                <form method="POST" action="{{route('admin.defaultSubscription.update', $premium->id)}}">
                                    @method('PUT')
                                    @csrf
                                    <input type="hidden" name="level" value="{{$premium->level}}">
                                    <h3 class="pricing__title">{{\Illuminate\Support\Str::upper($premium->level)}}</h3>
                                    <p class="pricing__sentence">Level Description here...</p>
                                    <div class="pricing__price"><span class="pricing__currency">$</span><input type="text" name="price" value="{{$premium->price}}" class="default-subscription-input"><span class="pricing__period">/ {{ ($premium->period==SUBSCRIPTION_EXPIRY_MONTHLY)? 'month':'year' }}</span></div>
                                    <ul class="pricing__feature-list text-center">
                                        @foreach(explode(',', $premium->features) as $feature)
                                            <li class="pricing__feature"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg> {{$feature}}</li>
                                        @endforeach
                                    </ul>
                                    <button type="submit" class="pricing__action mx-auto mb-4 btn-primary">Save Changes!</button>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection




