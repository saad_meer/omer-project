@extends('layouts.admin')
@section('title', 'Dashboard')
@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('admin-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('content')
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Add Product</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <h1>
                Product Information
            </h1>
            @if(isset($output['id']) && !empty($output['id']))

                <form method="post" action="{{route('admin.productUpdate')}}" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{$output['id']}}">
            @else
                <form method="post" action="{{route('admin.productADD')}}" enctype="multipart/form-data">
            @endif


                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="title">Product Title</label>
                                <input type="text" name="title" class="form-control" id="" value="{{old('title', $output['title'])}}"
                                       placeholder="Enter Product Title">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="short_description">Product Category</label>
                                <select class="form-control form-small" name="category_id">
                                    @foreach($Categories as $category)
                                        <option value="{{$category->id}}">{{$category->categoryTitle}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="price">Product Price</label>
                                <input type="number" name="price" class="form-control" id="" value="{{old('price', $output['price'])}}"
                                       placeholder="Enter Product Price">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="price">Product Quantity</label>
                                <input type="number" name="quantity" class="form-control" id="" value="{{old('quantity', $output['quantity'])}}"
                                       placeholder="Enter Product Quantity">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="status">Product Status</label>


                                <select class="form-control form-small" name="status">

                                    <option >Select Product Status</option>
                                    <option value="1" {{$output['status'] == 1 ? 'selected=selected' : ''}} >Active</option>
                                    <option value="0" {{$output['status'] == 0 ? 'selected=selected' : ''}}>IN-Active</option>
                                </select>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="short_description">Product Short Description</label>
                                <input type="text" name="short_description" class="form-control" id="" value="{{old('short_description', $output['short_description'])}}"
                                       placeholder="Enter Product Short Description">

                            </div>
                        </div>



                    </div>

                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Product Description</label>
                                <textarea class="form-control" name="description">{{old('description', $output['description'])}}</textarea>

                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class=" custom-file-container" data-upload-id="myFirstImage">
                                <label>Upload (Single File) <a href="javascript:void(0)"
                                                               class="custom-file-container__image-clear"
                                                               title="Clear Image">x</a></label>
                                <label class="custom-file-container__custom-file">
                                    <input type="file" name="image_url"
                                           class="custom-file-container__custom-file__custom-file-input"
                                           accept="image/*">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                </label>
                                @if(isset($output['image_url']) && $output['image_url'] != '')
                                    <style>
                                        .custom-file-container__image-preview {
                                            ox-sizing: border-box;
                                            transition: all 0.2s ease;
                                            margin-top: 54px;
                                            margin-bottom: 40px;
                                            height: 250px;
                                            width: 100%;
                                            border-radius: 4px;
                                            background-size: contain;
                                            background-position: center center;
                                            background-repeat: no-repeat;
                                            background-color: #fff;
                                            overflow: auto;
                                            padding: 15px;
                                        }
                                    </style>
                                    <!--                                    div m backgroud image lagae gy tu ok ho jae gaa kaam -->
                                    <img class="custom-file-container__image-preview" style="  margin-top: 54px;
                                        overflow: auto;
                                                padding: 15px;
                                                margin-bottom: 40px;
                                                height: 250px;
                                                width: 100%;
                                                border-radius: 4px;" src="{{asset($output['image_url'] ?? '')}}">
                                @else
                                    <div class="custom-file-container__image-preview"></div>
                                @endif


                            </div>
                        </div>

                    </div>


                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    </div>
@endsection

@section('footer')
    @parent
    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
    </script>
    <script>
        var formSmall = $(".form-small").select2({tags: true});
        formSmall.data('select2').$container.addClass('form-control-sm')
    </script>
@endsection
