@extends('layouts.admin')
@section('title', 'Dashboard')

@section('head')
    @parent
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin-dashboard/assets/css/forms/switches.css')}}">
    <link href="{{ asset('admin-dashboard/plugins/pricing-table/css/component.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
@endsection
@section('content')
    <div class="col-lg-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Membership Settings</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="container p-0">
                    <section class="pricing-section bg-7  mt-1">
                        <div class="pricing pricing--norbu  m-0">
                            @foreach($plans as $key=>$plan)
                                <div class="pricing__item mr-2 m-0">
                                    <form method="POST" action="{{route('admin.defaultMemberships.update', $defaultPlans[$key]->id)}}">
                                        @method('PUT')
                                        @csrf
                                        <h3 class="pricing__title">{{ \Illuminate\Support\Str::upper($defaultPlans[$key]->level) }}</h3>
                                        <p class="pricing__sentence"><textarea rows="3" name="description" class="form-control">{{$defaultPlans[$key]->description}}</textarea></p>
                                        <div class="pricing__price">
                                            <input type="hidden" name="price" value="{{number_format($plan->amount / 100, 2)}}">
                                            <input type="hidden" name="period" value="{{$plan->interval}}">
                                            <span class="pricing__currency">{{$plan->currency}}</span>{{number_format($plan->amount / 100, 2)}}<span class="pricing__period">/ {{ $plan->interval }}</span>
                                        </div>

                                        <p class="pricing__sentence">
                                            <label for="features">Features (Comma Separated)</label>
                                            <textarea rows="6" name="features" class="form-control">{{$defaultPlans[$key]->features}}</textarea>
                                        </p>
                                        <button type="submit" class="pricing__action mx-auto mb-4">Update</button>
                                    </form>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection




