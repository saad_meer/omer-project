@extends('layouts.main')
@section('title', 'Category')
@section('head')
    @parent
    <!--  <link rel="stylesheet" href="./style.css">-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <title>INSTAKTER</title>
    <style>
        .bbb_deals_featured {
            width: 100%;
        }

        .bbb_deals {
            width: 100%;
            margin-right: 7%;
            padding-top: 80px;
            padding-left: 25px;
            padding-right: 25px;
            padding-bottom: 34px;
            box-shadow: 1px 1px 5px 1px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
            margin-top: 0px;
        }

        .bbb_deals_title {
            position: absolute;
            top: 10px;
            left: 22px;
            font-size: 18px;
            font-weight: 500;
            color: #000000;
        }

        .bbb_deals_slider_container {
            width: 100%;
        }

        .bbb_deals_item {
            width: 100% !important;
        }

        .bbb_deals_image {
            width: 100%;
        }

        .bbb_deals_image img {
            width: 100%;
        }

        .bbb_deals_content {
            margin-top: 33px;
        }

        .bbb_deals_item_category a {
            font-size: 14px;
            font-weight: 400;
            color: rgba(0, 0, 0, 0.5);
        }

        .bbb_deals_item_price_a {
            font-size: 14px;
            font-weight: 400;
            color: rgba(0, 0, 0, 0.6);
        }

        .bbb_deals_item_price_a strike {
            color: red;
        }

        .bbb_deals_item_name {
            font-size: 24px;
            font-weight: 400;
            color: #000000;
        }

        .bbb_deals_item_price {
            font-size: 24px;
            font-weight: 500;
            color: #6d6e73;
        }

        .available {
            margin-top: 19px;
        }

        .available_title {
            font-size: 16px;
            color: rgba(0, 0, 0, 0.5);
            font-weight: 400;
        }

        .available_title span {
            font-weight: 700;
        }

        @media only screen and (max-width: 991px) {
            .bbb_deals {
                width: 100%;
                margin-right: 0px;
            }
        }

        @media only screen and (max-width: 575px) {
            .bbb_deals {
                padding-left: 15px;
                padding-right: 15px;
            }

            .bbb_deals_title {
                left: 15px;
                font-size: 16px;
            }

            .bbb_deals_slider_nav_container {
                right: 5px;
            }

            .bbb_deals_item_name,
            .bbb_deals_item_price {
                font-size: 20px;
            }
        }
        footer{
            position: absolute;
            width: 100%;
            bottom: 0;
        }
    </style>

@endsection
@section('content')
    <br>
    @if(session('success'))

        <div class="container">
            <div class="col-md-12">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>

        </div>


    @endif
    <br>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <table id="cart" class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th style="width:50%">Product</th>
                            <th style="width:10%">Price</th>
                            <th style="width:8%">Quantity</th>
                            <th style="width:22%" class="text-center">Subtotal</th>
                            <th style="width:10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $total = 0 @endphp

                        @if(session('cart'))

                            @foreach(session('cart') as $id => $details)

                                @php $total += $details['price'] * $details['quantity'] @endphp

                                <tr data-id="{{ $id }}">

                                    <td data-th="Product">

                                        <div class="row">

                                            <div class="col-sm-3 hidden-xs"><img src="{{ $details['image'] }}" width="100" height="100" class="img-responsive"/></div>

                                            <div class="col-sm-9">

                                                <h4 class="nomargin">{{ $details['name'] }}</h4>

                                            </div>

                                        </div>

                                    </td>

                                    <td data-th="Price">${{ $details['price'] }}</td>

                                    <td data-th="Quantity">

                                        <input type="number" min="1" value="{{ $details['quantity'] }}" class="form-control quantity update-cart" />

                                    </td>

                                    <td data-th="Subtotal" class="text-center">${{ $details['price'] * $details['quantity'] }}</td>

                                    <td class="actions" data-th="">

                                        <button class="btn btn-danger btn-sm remove-from-cart"><i class="fas fa-trash"></i></button>

                                    </td>

                                </tr>

                            @endforeach

                        @endif

                        </tbody>

                        <tfoot>

                        <tr>

                            <td colspan="5" class="text-right"><h3><strong>Total ${{ $total }}</strong></h3></td>

                        </tr>

                        <tr>

                            <td colspan="5" class="text-right">

                                <a href="{{ url('/') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a>

                                <a href="{{url('/checkout')}}" class="btn btn-success">Checkout</a>

                            </td>

                        </tr>

                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </section>




@endsection

@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script  src="{{asset('js/jquery-3.1.1.min.js')}}"  ></script>
    <script src="{{asset('js/bootstrap.min.js')}}"  ></script>
    <script type="text/javascript">



        $(".update-cart").change(function (e) {

            e.preventDefault();



            var ele = $(this);



            $.ajax({

                url: '{{ route('update.cart') }}',

                method: "patch",

                data: {

                    _token: '{{ csrf_token() }}',

                    id: ele.parents("tr").attr("data-id"),

                    quantity: ele.parents("tr").find(".quantity").val()

                },

                success: function (response) {

                    window.location.reload();

                }

            });

        });



        $(".remove-from-cart").click(function (e) {

            e.preventDefault();



            var ele = $(this);



            if(confirm("Are you sure want to remove?")) {

                $.ajax({

                    url: '{{ route('remove.from.cart') }}',

                    method: "DELETE",

                    data: {

                        _token: '{{ csrf_token() }}',

                        id: ele.parents("tr").attr("data-id")

                    },

                    success: function (response) {

                        window.location.reload();

                    }

                });

            }

        });

    <!-- END PAGE LEVEL SCRIPTS -->
    </script>
@endsection

  
