@extends('layouts.main')
@section('title', 'Category')
@section('head')
    @parent
    <!--  <link rel="stylesheet" href="./style.css">-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <title>INSTAKTER</title>
    <script src="https://www.paypal.com/sdk/js?client-id=AU7wwFkFdvWNGEkhAGvRAOHQcShs3fkZRyIU45S7OZrDAEu594Erhb--8axPEABqnOCX6hJg5JM0ePmV&currency=USD&disable-funding=credit"></script>
{{--    <script src="https://www.paypal.com/sdk/js?client-id=AU7wwFkFdvWNGEkhAGvRAOHQcShs3fkZRyIU45S7OZrDAEu594Erhb--8axPEABqnOCX6hJg5JM0ePmV&currency=USD"></script>--}}
{{--    <script src="https://www.paypal.com/sdk/js?client-id=YOUR_ID&disable-funding=credit"></script>--}}
@endsection
@section('content')
    <br><br>
    <section class="place-order">

        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="order-box order-info">
                        <h1><span class="number-1">1</span>Your Info</h1>
                    </div>
                    <form class="px-4" id="addpage" action="{{url('create_order')}}" method="POST">
                        <label>Phone Nmber</label>
                        <input type="text" class="phone-num" id="phone_num"  placeholder="(555)-23213-231">


                        <div class="order-box">
                            <h1><span class="number-1">2</span>Your Order</h1>
                            <br>
                            <label>First Name </label>
                            <input type="text" class="first_name" id="first_name" placeholder="First Name">
                            <label>Last Name</label>
                            <input type="text" class="last_name"  id="last_name"placeholder="Last Name">
                            <label>Email</label>
                            <input type="email" class="email"     id="email"placeholder="Email">
                        </div> 
                    </form>
                    <input type="hidden" id="transaction_id">
                    <input type="hidden" id="transaction_status">
                </div>
                <div class="col-sm-5 py-5 right-box">

                    <h2>order summary</h2>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $total = 0 @endphp

                        @if(session('cart'))

                            @foreach(session('cart') as $id => $details)
                                @php $total += $details['price'] * $details['quantity'] @endphp
                                <tr>
                                    <td>{{ $details['name'] }}</td>
                                    <td>
                                        {{-- <i class="fa fa-minus"></i> --}}
                                        <span>{{ $details['quantity'] }}</span>
                                        {{-- <i class="fa fa-plus"></i> --}}
                                    </td>
                                    <td>${{ $details['price'] }}</td>
                                </tr>

                            @endforeach

                        @endif
                        </tbody>
                    </table>


                    <table class="table sub-total-table">

                        <tbody>
                        <tr>
                            <td colspan="3">Subtotal </td>
                            <td colspan="2">${{ $total }}</td>
                        </tr>
                        {{-- <tr>
                            <td colspan="3">Taxes & Fees</td>
                            <td colspan="2">$10.8</td>
                        </tr> --}}
                        {{-- <tr>
                            <td colspan="5"><input placeholder="Promo Code" class="propmo-code"/></td>
                        </tr>
                        <tr>
                            <td>Support our team</td>
                            <td>4$</td>
                            <td>5$</td>
                            <td>8$</td>
                            <td>Custom</td>
                        </tr>
                        <tr>
                            <td colspan="5"><input placeholder="$3" class="propmo-code"/></td>
                        </tr> --}}
                        <tr>
                            <hr>
                            <td colspan="3"><h2>Grand Total</h2></td>
                            <td colspan="2"><b>${{ $total }}</b></td>
                        </tr>
                        {{-- <tr>
                            <td colspan="5"><a href="#" class="place-order">place  order</a></td>
                        </tr> --}}

                        </tbody>
                    </table>
                    <div class="order-box">
                        <h1><span class="number-1"></span>Payment</h1>
                        <br>

                        <!-- Set up a container element for the button -->
                        <div id="paypal-button-container"></div>

                    </div>

                </div>
            </div>
        </div>

    </section>
    <br>
@endsection

@section('footer')
        @parent
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script> --}}
        <script  src="{{asset('js/jquery-3.1.1.min.js')}}"  ></script>
        <script  src="{{asset('js/popper.min.js')}}"  ></script>
        <script src="{{asset('js/bootstrap.min.js')}}"  ></script>

        <script>
            var amount = '<?php echo $total;?>';
            paypal.Buttons({

                // Sets up the transaction when a payment button is clicked
                createOrder: function(data, actions) {
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: '77.44' // Can reference variables or functions. Example: `value: document.getElementById('...').value`
                            }
                        }]
                    });
                },

                // Finalize the transaction after payer approval
                onApprove: function(data, actions) {
                    return actions.order.capture().then(function(orderData) {
                        // Successful capture! For dev/demo purposes:
                        console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                        var transaction = orderData.purchase_units[0].payments.captures[0];
                        // alert('Transaction '+ transaction.status + ': ' + transaction.id + '\n\nSee console for all available details');
                        $("#transaction_id").val(transaction.id);
                        $("#transaction_status").val(transaction.status);
                        createorder();
                        // When ready to go live, remove the alert and show a success message within this page. For example:
                        // var element = document.getElementById('paypal-button-container');
                        // element.innerHTML = '';
                        // element.innerHTML = '<h3>Thank you for your payment!</h3>';
                        // Or go to another URL:  actions.redirect('thank_you.html');
                    });
                }
            }).render('#paypal-button-container');
            function  createorder()
            {
                var url = '{{url("/")}}';
                var transaction_id =   $("#transaction_id").val();
                var transaction_status = $("#transaction_status").val();

                var phone_num = $("#phone_num").val();
                var first_name = $("#first_name").val();
                var last_name = $("#last_name").val();
                var email = $("#email").val();
                $.ajax({
                    type : "post",
                    dataType : "json",
                    url : url+'/create_order',
                    data : {"_token": "{{ csrf_token() }}",
                        'transaction_id':transaction_id,
                        'transaction_status':transaction_status,
                        'phone_num':phone_num,
                        'first_name':first_name,
                        'last_name':last_name,
                        'email':email,
                    },
                    success: function(data) {
                        console.log(data);
                        if(data.code==200)
                        {
                            window.location.href=url+'/'
                        }
                    }
                })
            }

        </script>
    @endsection


