@extends('layouts.vendor')
@section('title', 'Dashboard')

@section('head')
    @parent
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('vendor-dashboard/assets/css/forms/switches.css')}}">
    <link href="{{ asset('vendor-dashboard/plugins/pricing-table/css/component.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor-dashboard/assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('vendor-dashboard/assets/css/components/custom-modal.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
@endsection
@section('page-title', 'Membership Package Plan')
@section('content')
    <div class="col-lg-12 layout-spacing">

        <div class="statbox widget box box-shadow">
            <div class="widget-content widget-content-area">
                <div class="container">
                    <section class="pricing-section bg-7 mt-5">
                        <div class="pricing pricing--norbu mx-0">
                                @foreach($plans as $key=>$plan)
                                <div class="pricing__item mr-0 {{ ($subscription ?? '' and $subscription->stripe_price == $plan->id)? 'price-selected' : ''  }}">
                                    <form method="POST"  action="{{route('membership.store')}}">
                                        @csrf
                                        <input type="hidden" name="plan" value="{{$plan->id}}">
                                        <input type="hidden" class="stripe_pm" name="stripe_pm" value="">
                                        <h3 class="pricing__title">{{ \Illuminate\Support\Str::upper($defaultPlans[$key]->level) }}</h3>
                                        <p class="pricing__sentence">{{$defaultPlans[$key]->description}}</p>
                                        <div class="pricing__price">
                                            <span class="pricing__currency">{{$plan->currency}}</span>{{number_format($plan->amount / 100, 2)}}
                                            <span class="pricing__period">/ {{ $plan->interval }}</span>
                                        </div>
                                        @if( $subscription ?? '' and $subscription->stripe_price == $plan->id)
                                            <p class="badge-warning subscription-info-bar">{{ ($subscription->stripe_status == 'active')? 'Purchased': "Expired!" }}</p>
                                        @endif
                                        <ul class="pricing__feature-list text-center">
                                            @foreach(explode(',', $defaultPlans[$key]->features) as $feature)
                                                <li class="pricing__feature"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg> {{$feature}}</li>
                                            @endforeach
                                        </ul>
                                        <button type="button" class="pricing__action mx-auto mb-4">Purchase</button>
                                    </form>
                                </div>
                            @endforeach
                        </div>
                    </section>

                    <!-- Modal -->
                    <div class="modal fade" id="cardDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Card Details</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-danger text-center card-errors"></div>
                                    <br>
                                    <input id="card-holder-name" placeholder="Card holder name" class="form-control" type="text"> <br>
                                    <!-- Stripe Elements Placeholder -->
                                    <div id="card-element"></div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                                    <button type="button" class="btn btn-primary" id="card-button" data-secret="{{ $intent->client_secret }}">
                                        Update!
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    @parent
    <script src="{{ asset('vendor-dashboard/assets/js/scrollspyNav.js')}}"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var currentForm = null;
        $('.pricing__action').click(function(){
            $('.pricing__item').removeClass('price-selected');
            $(this).closest('.pricing__item').addClass('price-selected');
            currentForm = $(this).closest("form")[0];
            $('#cardDetail').modal('show');
        });

        const stripe = Stripe('{{env('STRIPE_KEY')}}');
        const elements = stripe.elements();

        var style = {
            base: {
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
        var cardElement = elements.create('card', {hidePostalCode: true,
            style: style});
        cardElement.mount('#card-element');

        const cardHolderName = document.getElementById('card-holder-name');
        const cardButton = document.getElementById('card-button');
        const clientSecret = cardButton.dataset.secret;

        cardButton.addEventListener('click', async (e) => {
            const { setupIntent, error } = await stripe.confirmCardSetup(
                clientSecret, {
                    payment_method: {
                        card: cardElement,
                        billing_details: { name: cardHolderName.value }
                    }
                }
            );

            if (error) {
                console.log(error);
                $('.card-errors').html(error['message']);
            } else {
                currentForm.elements["stripe_pm"].value = setupIntent.payment_method;
                currentForm.submit();
            }
        });
    </script>
@endsection




