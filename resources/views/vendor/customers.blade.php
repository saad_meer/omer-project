@extends('layouts.vendor')
@section('title', 'Dashboard')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('page-title', 'My Customers')
@section('content')
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Customers</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                    <table id="vendor_cusomers_dt"
                           class="table table-bordered"
                           style="width:100%;">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Vendor</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Language</th>
                            <th>Payment Status</th>
                            <th>Is Guest?</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
        $(document).ready(function () {

            var table = $('#vendor_cusomers_dt').DataTable({

                //orderCellsTop: true,
                fixedHeader: true,
                processing: true,
                serverSide: true,
                ajax: "{{route('CustomerListing')}}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'vendor_name', name: 'vendors.name'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone', name: 'phone'},
                    {data: 'language', name: 'language'},
                    {data: 'payment_status', name: 'payment_status'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {"targets": 0, "width": "100px", "className": "ta-c"},
                    {"targets": 5, "width": "140px", "className": "ta-c"}
                ]
            });


        });


    </script>
@endsection
