@extends('layouts.vendor')
@section('title', 'Fill company details')
@section('page-title', 'Update Company Details')
@section('content')
    <!--  BEGIN CONTENT AREA  -->
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <br>
            <div class="account-content">
                 <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                     data-offset="-100">
                    <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                        <div class="col-sm-6 mx-auto p-0 pb-3"><h4>Update Access Token</h4></div>
                        <form action="{{url('updateAccessToken').'/'.$vendor->id}}">
                            <div class="info">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 mx-auto">
                                                <div class="form-group">
                                                    <label for="address">Client Id</label>
                                                    <input type="text" readonly class="form-control mb-4"
                                                           placeholder="Client ID" value="{{ $vendor->client_id }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6 mx-auto">
                                                <label>Access Token</label>
                                                <div class="input-group mb-3 token_holder">
                                                    <input id="client_token" type="text"
                                                           value="{{$vendor->access_token}}" class="form-control">
                                                    <div class="input-group-append pr-1 " >
                                                        <button class="btn-primary  bs-tooltip"   style="width: 50px;"
                                                                title="Copy to Clipboard"
                                                                type="button"
                                                                onclick="copyToken()"><i style="font-size: 1.3rem;" class="far fa-clone "></i>
                                                        </button>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-6 mx-auto account-settings-footer  text-right">
                                        <br>
                                        <div class="as-footer-container">
                                            <button type="submit" class="btn btn-primary">Regenrate Token</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->
    <script>
        function copyToken() {

            let copyText = document.getElementById("client_token");
            let textToCopy = copyText.value.trim();


            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            if (textToCopy.length === 0) {
                alert('No data available to be copied');
                return false;
            }

            // navigator clipboard api needs a secure context (https)
            if (navigator.clipboard && window.isSecureContext) {
                // navigator clipboard api method'
                alert('copied to clipboard');
                return navigator.clipboard.writeText(textToCopy);
            } else {

                // text area method
                let textArea = document.createElement("textarea");
                textArea.value = textToCopy;
                // make the textarea out of viewport
                textArea.style.position = "fixed";
                textArea.style.left = "-999999px";
                textArea.style.top = "-999999px";
                document.body.appendChild(textArea);
                textArea.focus();
                textArea.select();
                alert('coppied to clipboard');

                return new Promise((res, rej) => {
                    // here the magic happens
                    document.execCommand('copy') ? res() : rej();
                    textArea.remove();
                });
            }


        }

    </script>

@endsection
