<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
{{--<script src="{{ asset('vendor-dashboard/assets/js/loader.js') }}"></script>--}}

<script src="{{ asset('vendor-dashboard/plugins/blockui/jquery.blockUI.min.js') }}"></script>
<script src="{{ asset('vendor-dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('vendor-dashboard/plugins/highlight/highlight.pack.js') }}"></script>
<script src="{{ asset('vendor-dashboard/assets/js/app.js') }}"></script>
<script src="{{ asset('vendor-dashboard/assets/js/custom.js') }}"></script>
<script src="{{ asset('toastr/toastr.min.js') }}"></script>

<script src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/js/file-upload-with-preview.min.js')}}"></script>

<script src="{{asset('vendor-dashboard/plugins/sweetalerts/sweetalert2.min.js')}}"></script>
<script src="{{asset('vendor-dashboard/plugins/sweetalerts/custom-sweetalert.js')}}"></script>

<script>
    $(document).ready(function() {
        App.init();
    });

</script>

