<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>

<link rel="icon" type="image/x-icon" href="{{ asset('vendor-dashboard/assets/img/favicon.ico') }}"/>
<link href="{{ asset('vendor-dashboard/assets/css/loader.css') }}" rel="stylesheet" type="text/css" />

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/file-upload-with-preview.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('vendor-dashboard/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendor-dashboard/assets/css/structure.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendor-dashboard/plugins/font-icons/fontawesome/css/fontawesome.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendor-dashboard/plugins/font-icons/fontawesome/css/regular.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendor-dashboard/assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />

-->
<link href="{{ asset('assets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<link href="{{asset('vendor-dashboard/plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('vendor-dashboard/assets/css/components/custom-sweetalert.css')}}" rel="stylesheet"
      type="text/css"/>
<style>
                .swal2-icon.swal2-warning {
                    align-items: center;
                    width: 100px;
                    height: 100px;
                    margin: 20px;
                }
                .swal2-icon-text{
                    font-size: 100px !important;
                }
            </style>

<!-- END GLOBAL MANDATORY STYLES -->
