@extends('layouts.vendor')
@section('title', 'Dashboard')
@section('content') 
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Orders id #{{$order->id}}</h4>
                        <p><strong>Name:</strong>{{$order->billing_first_name.' '.$order->billing_last_name}}</p>
                        <p><strong>Email:</strong>{{$order->billing_email}}</p>
                        <p><strong>Phone:</strong>{{$order->billing_phone}}</p>
                        <div class="rounded">
                            <div class="table-responsive table-borderless">
                                <table class="table">
                                    <thead>
                                        <tr> 
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Product Price</th>
                                            <th>Total</th>  
                                        </tr>
                                    </thead>
                                    <tbody class="table-body">
                                        @foreach($order->orderItems as $value) 
                                            <tr class="cell-1">  
                                                <td>{{$value->product_name}}</td>
                                                <td>{{$value->quantity}}</td>
                                                <td>{{$value->total}}</td>
                                                <td>{{$value->quantity*$value->total}}</td>  
                                            </tr>
                                        @endforeach
                                       <tr>
                                           <td>Total</td>
                                           <td>{{$order->order_total}}</td>
                                       </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
@endsection

@section('footer')
    

@endsection
