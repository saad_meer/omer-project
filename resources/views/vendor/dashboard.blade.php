@extends('layouts.vendor')
@section('title', 'Dashboard')

@section('head')
@parent
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="{{asset('vendor-dashboard/plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('vendor-dashboard/assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" class="dashboard-sales" />
<!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
@endsection
@section('page-title', 'My Dashboard')
@section('content')
    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
        <div class="widget-three">
            <div class="widget-heading">
                <h5 class="">Summary</h5>
            </div>
            <div class="widget-content">

                <div class="order-summary">

                    <div class="summary-list">
                        <div class="w-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
                        </div>
                        <div class="w-summary-details">

                            <div class="w-summary-info">
                                <h6>Total orders</h6>
                                <p class="summary-count">{{$totalOrders}}</p>
                            </div>

                            <div class="w-summary-stats">
                                <div class="progress">
                                    <div class="progress-bar bg-gradient-secondary" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="summary-list">
                        <div class="w-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7" y2="7"></line></svg>
                        </div>
                        <div class="w-summary-details">

                            <div class="w-summary-info">
                                <h6>Scheduled Orders</h6>
                                <p class="summary-count">{{$scheduledOrders}}</p>
                            </div>

                            <div class="w-summary-stats">
                                <div class="progress">
                                    <div class="progress-bar bg-gradient-success" role="progressbar" style="width: 65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                        </div>

                    </div>



                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
        <div class="widget-two">
            <div class="widget-content">
                <div class="w-numeric-value">
                    <div class="w-content">
                        <span class="w-value">Daily sales</span>
                        <span class="w-numeric-title">Go to columns for details.</span>
                    </div>
                    <div class="w-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                    </div>
                </div>
                <div class="w-chart">
                    <div id="daily-sales"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget-one widget">
            <div class="widget-content">
                <div class="w-numeric-value">
                    <div class="w-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                    </div>
                    <div class="w-content">
                        <span class="w-value">${{$totalRevenue}}</span>
                        <span class="w-numeric-title">Total Revenue</span>
                    </div>
                </div>
                <div class="w-chart">
                    <div id="total-orders"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget dashboard-widget-table">

            <div class="widget-heading">
                <h5 class="">Recent Orders</h5>
            </div>

            <div class="widget-content">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Identifier</th>
                            <th>Customer</th>
                            <th>Shipping Cost</th>
                            <th>Total Price</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                            <th class="text-center">Created_at</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{-- @foreach($orders as $order) 
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->identifier}}</td>
                                <td>{{$order->customer->name}}</td>
                                <td>{{$order->shipping_cost}}</td>
                                <td>{{$order->order_total}}</td>
                                <td>
                                    @if($order->status)
                                        <span class="shadow-none badge badge-primary">Completed</span>
                                    @else
                                        <span class="shadow-none badge badge-warning">Pending</span>
                                    @endif
                                </td>
                                <td class="text-center"><button class="btn btn-outline-primary"><a href="/customer/{{$order->id}}">View</a></button></td>
                                <td>{{$order->created_at}}</td>
                            </tr>
                        @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
        <div class="widget dashboard-widget-table">

            <div class="widget-heading">
                <h5 class="">Orders to be Scheduled</h5>
            </div>

            <div class="widget-content">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Identifier</th>
                            <th>Period</th>
                            <th>Schedule</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                            <th class="text-center">Created_at</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orderSchedules as $orderSchedule)
                            <tr>
                                <td>{{$orderSchedule->id}}</td>
                                <td>{{$orderSchedule->order->identifier}}</td>
                                <td>{{$orderSchedule->period}}</td>
                                <td>{{$orderSchedule->period_datetime}}</td>
                                <td>
                                    @if($orderSchedule->status)
                                        <span class="shadow-none badge badge-primary">Shipped</span>
                                    @else
                                        <span class="shadow-none badge badge-danger">Pending</span>
                                    @endif
                                </td>
                                <td class="text-center"><button class="btn btn-outline-primary"><a href="/customer/{{$orderSchedule->id}}">View</a></button></td>
                                <td>{{$orderSchedule->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
@parent
<script src="{{asset('vendor-dashboard/plugins/apex/apexcharts.min.js')}}"></script>
<script src="{{asset('vendor-dashboard/assets/js/dashboard/dash_2.js')}}"></script
@endsection
