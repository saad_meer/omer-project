@extends('layouts.vendor')
@section('title', 'Dashboard')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('page-title', 'Widget Settings')
@section('content')
    <!--  BEGIN CONTENT AREA  -->
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <br>
            <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form method="POST"  action="/widget/{{$widget->id}}">
                                    @method('PUT')
                                    @csrf
                                    <div class="info">
                                        <div class="row">
                                            <div class="col-md-8 mx-auto">
                                                <div class="work-section">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="title">Widget Title</label>
                                                                <input type="text" name="title" class="form-control mb-4" id="title" placeholder="Your Widget Title" value="{{$widget->title}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="description">Widget Description</label>
                                                                <input type="text" name="description" class="form-control mb-4" id="title" placeholder="Your Widget Description" value="{{$widget->description}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="width">Width</label>
                                                                        <input type="text"  name="width" class="form-control mb-4" id="degree3" placeholder="Add your work here" value="{{$widget->width}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="height">Height</label>
                                                                        <input type="text" name="height" class="form-control mb-4" id="height" placeholder="Your widget height" value="{{$widget->height}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <label for="content">Widget Code</label>
                                                            <textarea class="form-control" placeholder="Description" rows="10">{!!html_entity_decode($widget->content)!!}</textarea>
                                                        </div>
                                                        <div class="col-md-12 account-settings-footer  text-right">
                                                            <br>
                                                            <div class="as-footer-container">
                                                                <button id="widget-copy-code" class="btn btn-dark">Copy the widget code</button>
                                                                <button id="reset-all" class="btn btn-warning">Reset All</button>
                                                                <button id="save-changes" type="submit" class="btn btn-primary">Save Changes</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->
@endsection

@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
