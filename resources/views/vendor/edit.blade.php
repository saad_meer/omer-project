@extends('layouts.vendor')
@section('title', 'Fill company details')
@section('page-title', 'Update Company Details')

@section('content')
    <!--  BEGIN CONTENT AREA  -->
    <div class="col-lg-12">
        @if(session()->has('message'))
            <div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></button>
                {{ session()->get('message') }}</button>
            </div>
        @endif
        <div class="statbox widget box box-shadow">
            <br>
            <div class="account-content">
                <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                     data-offset="-100">

                    <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
<!--                        <form method="POST" action="{ {url('vendor').'/'.$vendor->id}}">-->
                        <form method="POST" action="{{route('updateVendorSettings')}}" enctype="multipart/form-data">
<!--                            @ method('PUT')-->
                            @csrf
                            <div class="info">
                                <div class="row">

                                    <div class="col-md-6 mx-auto">
                                        <div class="work-section">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="title">Full Name</label>
                                                            <input type="text" name="title"  class="form-control mb-2"
                                                                   id="title" placeholder="Add your Company name here"
                                                                   value="{{old('title', $vendor->title)}}">
                                                            <!--                                                        value="{ { $vendor->title }}"-->
                                                            @if($errors->has('title'))
                                                                <div
                                                                        class="error text-danger">{{ $errors->first('title') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="email">Email</label>
                                                            <input type="email" name="email"
                                                                   class="form-control mb-2" id="email"
                                                                   placeholder="Company email id"
                                                                   value="{{old('email', $vendor->email)}}">
                                                            @if($errors->has('email'))
                                                                <div
                                                                        class="error text-danger">{{ $errors->first('email') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="phone">Mobile Phone</label>
                                                            <input type="tel" name="phone"
                                                                   class="form-control mb-2" id="email"
                                                                   placeholder="Add your Company Phone number here"
                                                                   value="{{old('phone', $vendor->phone)}}">
                                                            @if($errors->has('phone'))
                                                                <div
                                                                        class="error text-danger">{{ $errors->first('phone') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 account-settings-footer  text-left">
                                                        <br>
                                                        <div class="as-footer-container">
                                                            <button id="multiple-messages" class="btn btn-primary">Update!
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="col-md-12">
                                                        <div class=" custom-file-container" data-upload-id="myFirstImage">
                                                            <label>Upload (Single File) <a href="javascript:void(0)"
                                                                                           class="custom-file-container__image-clear"
                                                                                           title="Clear Image">x</a></label>
                                                            <label class="custom-file-container__custom-file">
                                                                <input type="file" name="profile_image"
                                                                       class="custom-file-container__custom-file__custom-file-input"
                                                                       accept="image/*">
                                                                <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                                                                <span class="custom-file-container__custom-file__custom-file-control"></span>
                                                            </label>
                                                            @if(isset($vendor->profile_image) && $vendor->profile_image != '')
                                                                <style>
                                                                    .custom-file-container__image-preview {
                                                                        ox-sizing: border-box;
                                                                        transition: all 0.2s ease;
                                                                        margin-top: 54px;
                                                                        margin-bottom: 40px;
                                                                        height: 250px;
                                                                        width: 100%;
                                                                        border-radius: 4px;
                                                                        background-size: contain;
                                                                        background-position: center center;
                                                                        background-repeat: no-repeat;
                                                                        background-color: #fff;
                                                                        overflow: auto;
                                                                        padding: 15px;
                                                                    }
                                                                </style>
                                                                <!--                                    div m backgroud image lagae gy tu ok ho jae gaa kaam -->
                                                                <img class="custom-file-container__image-preview" style="  margin-top: 54px;
                                        overflow: auto;
                                                padding: 15px;
                                                margin-bottom: 40px;
                                                height: 250px;
                                                width: 100%;
                                                border-radius: 4px;" src="{{asset($vendor->profile_image ?? '')}}">
                                                            @else
                                                                <div class="custom-file-container__image-preview"></div>
                                                            @endif


                                                        </div>
                                                    </div>
                                                </div>


<!--                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="password">Password</label>
                                                                <input type="password" name="password"
                                                                       class="form-control mb-2" id="password"

                                                                      value="{  {old('password', $vendor->password)}}" >
                                                                @ if($errors->has('password'))
                                                                    <div
                                                                        class="error text-danger">{ { $errors->first('password') }}</div>
                                                                @ endif
                                                            </div>
                                                        </div>-->






                                   

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->

@endsection

@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
    </script>



@endsection