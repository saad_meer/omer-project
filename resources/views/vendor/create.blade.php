@extends('layouts.vendor')
@section('title', 'Fill company details')
@section('page-title', 'Add Your Company Details')
@section('content')
    <!--  BEGIN CONTENT AREA  -->
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">
            <br>
            <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form method="POST" action="{{route('vendor.store')}}" id="work-experience" class="section work-experience">
                                    @csrf
                                    <div class="info">
                                        <div class="row">
                                            <div class="col-md-6 mx-auto">
                                                <div class="work-section">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="title">Company Title</label>
                                                                <input type="text" required name="title" class="form-control mb-4" id="title" placeholder="Add your Company name here" value="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="email">Company Email</label>
                                                                        <input type="email" name="email" required class="form-control mb-4" id="email" placeholder="Company email id" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="web_url">Company Website URL</label>
                                                                        <input type="text" name="web_url" required class="form-control mb-4" id="web_url" placeholder="Company Website Address" value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="phone">Phone Number</label>
                                                                        <input type="tel" name="phone" required class="form-control mb-4" id="email" placeholder="Add your Company Phone number here" value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="address">Company Address</label>
                                                                        <input type="text" name="address" required class="form-control mb-4" id="address" placeholder="Add your Company Address" value="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <label for="about">About Company</label>
                                                            <textarea class="form-control" name="about" placeholder="Please write about your company" rows="3"></textarea>
                                                        </div>
                                                        <div class="col-md-12 account-settings-footer  text-right">
                                                            <br>
                                                            <div class="as-footer-container">
                                                                <button id="multiple-messages" class="btn btn-primary">Save!</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->
@endsection
