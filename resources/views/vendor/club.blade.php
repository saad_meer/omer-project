@extends('layouts.vendor')
@section('title', 'Dashboard')

@section('head')
    @parent



    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('page-title', 'My Clubs')
@section('content')
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">

            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-6">
                        <h4>Clubs</h4>
                    </div>

                    <div class="col-xl-6 col-md-6 col-sm-6 col-6" style="text-align: right;margin-top: 10px;">
                        <a class="btn btn-outline-primary" href="{{url('myclub/create')}}">Add Club</a>
                    </div>

                </div>

                @if(session()->has('message'))
                    <div class="alert alert-success mb-4" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none"
                                 stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round"
                                 class="feather feather-x close" data-dismiss="alert">
                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                <line x1="6" y1="6" x2="18" y2="18"></line>
                            </svg>
                        </button>
                        {{ session()->get('message') }}</button>
                    </div>
                @endif
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                                         <table id="vendor_clubs_dt"
                                                class="table table-bordered"
                                                style="width:100%;">
                                             <thead>
                                                 <tr>
                                                     <th>Record ID</th>
                                                     <th>Vendor Name</th>
                                                     <th>Club Title</th>
                                                     <th>Number of Products</th>
                                                     <th>Club Discounts</th>
                                                     <th>Action</th>
                                                 </tr>
                                             </thead>
                                             <tbody></tbody>
                                         </table>
                                     </div>
            </div>
        </div>
    </div>


@endsection



@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <script src="{{asset('vendor-dashboard/plugins/sweetalerts/promise-polyfill.js')}}"></script>
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>

    <!-- END PAGE LEVEL SCRIPTS -->

    <script src="{{asset('vendor-dashboard/plugins/table/datatable/datatables.js')}}"></script>
 <script type="text/javascript">
     $(document).ready(function() {

         var table = $('#vendor_clubs_dt').DataTable({

              //orderCellsTop: true,
              fixedHeader: true,
              processing: true,
              serverSide: true,
                ajax: "{{route('ClubListing')}}",
              columns: [
                  {data: 'id', name: 'id'},
                  {data: 'vendor_name', name: 'vendors.name'},
                  {data: 'club_title', name: 'club_title'},
                  {data: 'number_of_products', name: 'number_of_products'},
                  {data: 'club_discount', name: 'club_discount'},
                  {data: 'action', name: 'action', orderable: false, searchable: false},
              ],
              columnDefs :[
                  {"targets": 0 , "width": "100px" , "className": "ta-c"},
                  {"targets": 5 , "width": "140px", "className": "ta-c" }
              ]
          });



     });

     function delete_club(club_id) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                padding: '1em'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        url: "myclub/delete/" + club_id,
                        success: function (data) {
                            swal(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            );
                            //$('#club-table').DataTable().ajax.reload();
                            toastr.success('Club deleted Succesfully', {timeOut: 5000});
                            $('#vendor_clubs_dt').DataTable().ajax.reload();
                           /* setTimeout(function(){
                                window.location.reload(true);
                            }, 2000);*/
                            //window.location.reload(true);
                            //$(this).parent().remove();
                        }

                    });
                    //window.location.reload(true);
                    //window.open(url);
                }

            })


        }

</script>

@endsection
<!-- BEGIN PAGE LEVEL SCRIPTS -->

