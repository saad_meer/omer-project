@extends('layouts.vendor')
@section('title', 'Dashboard')

@section('head')
    @parent
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/custom_dt_miscellaneous.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('vendor-dashboard/plugins/table/datatable/dt-global_style.css') }}">
@endsection
@section('page-title', 'My Club')
@section('content')
    <!--  BEGIN CONTENT AREA  -->
    <div class="col-lg-12">
        @if(session()->has('message'))
            <div class="alert alert-success mb-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-x close" data-dismiss="alert">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
                {{ session()->get('message') }}</button>
            </div>
        @endif
        <div class="statbox widget box box-shadow">
            <br>
            <div class="account-content">
                <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                     data-offset="-100">
                    <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                        <form method="POST" action="{{route('myclub.store')}}" enctype="multipart/form-data">

                            @csrf
                            <div class="info">
                                <div class="row">
                                    <div class="col-md-8 mx-auto">
                                        <div class="work-section">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="club_title">Club Title</label>
                                                        <input type="text" name="club_title" class="form-control mb-2"
                                                               id="club_title" placeholder="Your Club Title"
                                                               value="{{ old('club_title') }}" >
                                                        @if($errors->has('club_title'))
                                                            <div
                                                                class="error text-danger">{{ $errors->first('club_title') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="number_of_products">Number of Products</label>
                                                        <input type="number" name="number_of_products"
                                                               class="form-control mb-2" id="number_of_products"
                                                               value="{{ old('number_of_products') }}"
                                                               placeholder="Number of Products">
                                                        @if($errors->has('number_of_products'))
                                                            <div
                                                                class="error text-danger">{{ $errors->first('number_of_products') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="club_discount">Product Discount</label>
                                                        <input type="text" name="club_discount"
                                                               class="form-control mb-2" id="club_discount" value="{{ old('club_discount') }}"
                                                               placeholder="Product Discount%">
                                                        @if($errors->has('club_discount'))
                                                            <div
                                                                class="error text-danger">{{ $errors->first('club_discount') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="club_image_url">Club Image</label>
                                                        <input type="file" name="club_image_url"  value="{{ old('club_image_url') }}"
                                                               class="form-control mb-2" id="club_image_url">
                                                        @if($errors->has('club_image_url'))
                                                            <div
                                                                class="error text-danger">{{ $errors->first('club_image_url') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12 account-settings-footer  text-right">
                                                    <br>
                                                    <div class="as-footer-container">
                                                        <button id="save-changes" type="submit" class="btn btn-primary">
                                                            Save Changes
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->
@endsection

@section('footer')
    @parent
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/datatables.js')}}"></script>
    <script src="{{asset('vendor-dashboard/plugins/table/datatable/custom_miscellaneous.js')}}"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
