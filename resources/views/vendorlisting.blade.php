@extends('layouts.main')
@section('title', 'Category')
@section('head')
  @parent
  <link rel="stylesheet" type="text/css" href="{{ asset('admin-dashboard/plugins/table/datatable/datatables.css') }}">
  <style>
    .bbb_deals_featured {
      width: 100%;
    }

    .bbb_deals {
      width: 100%;
      margin-right: 7%;
      padding-top: 80px;
      padding-left: 25px;
      padding-right: 25px;
      padding-bottom: 34px;
      box-shadow: 1px 1px 5px 1px rgba(0, 0, 0, 0.1);
      border-radius: 5px;
      margin-top: 0px;
    }

    .bbb_deals_title {
      position: absolute;
      top: 10px;
      left: 22px;
      font-size: 18px;
      font-weight: 500;
      color: #000000;
    }

    .bbb_deals_slider_container {
      width: 100%;
    }

    .bbb_deals_item {
      width: 100% !important;
    }

    .bbb_deals_image {
      width: 100%;
    }

    .bbb_deals_image img {
      width: 100%;
    }

    .bbb_deals_content {
      margin-top: 33px;
    }

    .bbb_deals_item_category a {
      font-size: 14px;
      font-weight: 400;
      color: rgba(0, 0, 0, 0.5);
    }

    .bbb_deals_item_price_a {
      font-size: 14px;
      font-weight: 400;
      color: rgba(0, 0, 0, 0.6);
    }

    .bbb_deals_item_price_a strike {
      color: red;
    }

    .bbb_deals_item_name {
      font-size: 24px;
      font-weight: 400;
      color: #000000;
    }

    .bbb_deals_item_price {
      font-size: 24px;
      font-weight: 500;
      color: #6d6e73;
    }

    .available {
      margin-top: 19px;
    }

    .available_title {
      font-size: 16px;
      color: rgba(0, 0, 0, 0.5);
      font-weight: 400;
    }

    .available_title span {
      font-weight: 700;
    }

    @media only screen and (max-width: 991px) {
      .bbb_deals {
        width: 100%;
        margin-right: 0px;
      }
    }

    @media only screen and (max-width: 575px) {
      .bbb_deals {
        padding-left: 15px;
        padding-right: 15px;
      }

      .bbb_deals_title {
        left: 15px;
        font-size: 16px;
      }

      .bbb_deals_slider_nav_container {
        right: 5px;
      }

      .bbb_deals_item_name,
      .bbb_deals_item_price {
        font-size: 20px;
      }
    }
  </style>
@endsection
@section('content')

  <section>
    <div class="container mydiv">

<br>
      <div class="row">

        @foreach($vendors as $vendor)

          <div class="col-md-4">
            <!-- bbb_deals -->
            <div class="bbb_deals">
              <div class="ribbon ribbon-top-right">
                <!--              <span>
                                <small class="cross">x </small>4
                              </span>-->
              </div>
              <div class="bbb_deals_title">Today's Combo Offer</div>
              <div class="bbb_deals_slider_container">
                <div class=" bbb_deals_item">
                  <div class="bbb_deals_image"><img src="{{asset($vendor->profile_image)}}" alt=""></div>
                  <div class="bbb_deals_content">
                    <div class="bbb_deals_info_line d-flex flex-row justify-content-start">
                      <div class="bbb_deals_item_category"><a href="#">{{$vendor->title}}</a></div>
                      <div class="bbb_deals_item_price_a ml-auto">
                        <!--                      <strike>₹30,000</strike>-->
                      </div>
                    </div>
                    <div class="bbb_deals_info_line d-flex flex-row justify-content-start">
                      <div class="bbb_deals_item_name">{{$vendor->name}}</div>
<!--                      <div class="bbb_deals_item_price ml-auto">{ {$vendor->title}}</div>-->
                    </div>
                    <div class="available">
                      <div class="available_line d-flex flex-row justify-content-start">
<!--                        <div class="available_title">Available: <span>{{$vendor->quantity}}</span></div>-->
                        <div class="sold_stars ml-auto"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                      </div>
                      <div class="available_bar"><span style="width:17%"></span></div>
                    </div>
                    <br>
                    <div class="col-md-12 d-flex justify-content-center">

                      <!--                    <a class="btn btn-lg btn-danger text-white" >Buy Now</a>-->
                      <a class="btn btn-lg btn-danger text-white" href="{{ route('vendorLists', $vendor->id) }}">View Products</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach


      </div>
    </div>
  </section>

  <br><br>
@endsection

@section('footer')
  @parent
  <!-- BEGIN PAGE LEVEL SCRIPTS -->

  <!-- END PAGE LEVEL SCRIPTS -->
@endsection
