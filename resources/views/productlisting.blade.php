@extends('layouts.main')
@section('title', 'Category')
@section('head')
    @parent
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('admin-dashboard/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .bbb_deals_featured {
            width: 100%;
        }

        .bbb_deals {
            width: 100%;
            margin-right: 7%;
            padding-top: 80px;
            padding-left: 25px;
            padding-right: 25px;
            padding-bottom: 34px;
            box-shadow: 1px 1px 5px 1px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
            margin-top: 0px;
        }

        .bbb_deals_title {
            position: absolute;
            top: 10px;
            left: 22px;
            font-size: 18px;
            font-weight: 500;
            color: #000000;
        }

        .bbb_deals_slider_container {
            width: 100%;
        }

        .bbb_deals_item {
            width: 100% !important;
        }

        .bbb_deals_image {
            width: 100%;
        }

        .bbb_deals_image img {
            width: 100%;
        }

        .bbb_deals_content {
            margin-top: 33px;
        }

        .bbb_deals_item_category a {
            font-size: 14px;
            font-weight: 400;
            color: rgba(0, 0, 0, 0.5);
        }

        .bbb_deals_item_price_a {
            font-size: 14px;
            font-weight: 400;
            color: rgba(0, 0, 0, 0.6);
        }

        .bbb_deals_item_price_a strike {
            color: red;
        }

        .bbb_deals_item_name {
            font-size: 24px;
            font-weight: 400;
            color: #000000;
        }

        .bbb_deals_item_price {
            font-size: 24px;
            font-weight: 500;
            color: #6d6e73;
        }

        .available {
            margin-top: 19px;
        }

        .available_title {
            font-size: 16px;
            color: rgba(0, 0, 0, 0.5);
            font-weight: 400;
        }

        .available_title span {
            font-weight: 700;
        }

        @media only screen and (max-width: 991px) {
            .bbb_deals {
                width: 100%;
                margin-right: 0px;
            }
        }

        @media only screen and (max-width: 575px) {
            .bbb_deals {
                padding-left: 15px;
                padding-right: 15px;
            }

            .bbb_deals_title {
                left: 15px;
                font-size: 16px;
            }

            .bbb_deals_slider_nav_container {
                right: 5px;
            }

            .bbb_deals_item_name,
            .bbb_deals_item_price {
                font-size: 20px;
            }
        }
    </style>
@endsection
@section('content')


    <section class="listing-form">
         
        <div class="container">

            <div class="row py-3 mb-5">
                <div class="col-sm-3 checkout-btns">
                    <label>Order Type</label>
                    <div class="form-group">
                        <a class="delivery" href="#">Delivery</a>
                        <a class="checkout" href="#">Checkout</a>
                    </div>
                </div>
                <div class="col-sm-3 updated-address">
                    <label>Updated Address</label>
                    <div class="form-group">
                        <input type="text" class="listing-input" placeholder="Address" />
                        <a class="save-btn" href="#">Save</a>
                    </div>
                </div>
                <div class="col-sm-3 delivery-date">
                    <label>Delivery Date</label>
                    <div class="form-group">
                        <input type="text" class="listing-input" placeholder="calendar" />
                    </div>
                </div>
                <div class="col-sm-3 delivery-date">
                    <label>How Many People</label>
                    <div class="form-group">
                        <input type="text" class="listing-input" placeholder="Address" />
                        <a class="save-btn" href="#">Save</a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="listing-categories">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="listing-heading">Listing Categories</h2>
                </div>
            </div>
<!--            <div class="row py-5 " >
                @ foreach($vendors as $vendor)

                <div class=" col-md-2 col-6 py-2 mr-2 text-center" style=' background-repeat: no-repeat;background-size: cover;background-position: center; background-image: url("{ {asset('/').$vendor->profile_image}}")' >
                  <a href="{ { route('vendorLists', $vendor->id) }}">
                       <span class="listing-icons">
                        { {&#45;&#45; <i class="fa fa-coffee"></i> &#45;&#45;}}
                        <p>{ {$vendor->title}}</p>
                    </span>

                  </a>
                </div>
              @ endforeach

            </div>-->

            <div class="row py-5">
        <?php
        // Asain BBQ Breakfast Pizza Mediter Sandwich


             foreach($categories as $category){

                 if(isset($category->category_id) && $category->category_id ==1 ){
                     $fa = '<i class="fa fa-coffee"></i>';
                 }
                 elseif(isset($category->category_id) && $category->category_id ==2 ){
                     $fa = '<i class="fa fa-fire"></i>';
                 }
                 elseif(isset($category->category_id) && $category->category_id ==3 ){
                     $fa = '<i class="fa fa-cutlery"></i>';
                 }
                 elseif(isset($category->category_id) && $category->category_id ==4 ){
                     $fa = '<i class="fa fa-venus-double"></i>';
                 }
                 elseif(isset($category->category_id) && $category->category_id ==5 ){
                     $fa = '<i class="fa fa-empire"></i>';
                 }
                 elseif(isset($category->category_id) && $category->category_id ==6 ){
                     $fa = '<i class="fa fa-database"></i>';
                 }

                 echo '<div class=" col-md-2 col-6 py-2 text-center"><a href="'.url('INSTAKTER/' . $category->vendor_id . '/' . $category->category_id).'" >

                            <span class="listing-icons">
                                '.$fa.'
                                <p style="color:black;">'.$category->categoryTitle.'</p>
                            </span>

                    </a></div>';

             }
        ?>
            </div>








        </div>
    </section>


    <section>
        <div class="container">
            <div class="row py-5">
                <div class="col-md-3 search-bar">

                    <label>Search</label>
<!--                    <input type="text" placeholder="Search">-->
                    <select class="form-control" id="searchbox_vendor">

                        @foreach($vendors as $val)
                            <option value="{{$val->id}}" {{( isset($selectedVendor) && $selectedVendor == $val->id ? 'selected="selected"' : '' )}}>
                                {{$val->title}}
                            </option>
                        @endforeach

                    </select>






                    <div class="left-box">
                        <h5>#1 ONLINE CATERING SITE</h5>
                        <p>
                            INSTAKTER is the #1 site for ordering food for business, with over 192,016,185 people served,
                            including half of the Fortune 500.
                        </p>
                    </div>
                    <div class="left-box">
                        <h5>#1 ONLINE CATERING SITE</h5>
                        <p>
                            INSTAKTER is the #1 site for ordering food for business, with over 192,016,185 people served,
                            including half of the Fortune 500.
                        </p>
                    </div>
                </div>


                <div class="col-md-9">
                    @foreach($products as $product)
                        <div   data-product-id="{{$product->products_id}}"  class="select listing-box show_product_popUp">
                            <img src="{{asset($product->image_url)}}">
                            <p class="image-text">5X Rewards</p>
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <h3>{{$product->products_company}}</h3>
                                        <p>{{$product->vendor_title}}</p>
                                        <p>Category : {{$product->category}}</p>
                                        <p>${{$product->price}}</p>
                                        <span class="last-order">Stock : {{$product->quantity}}</span>
                                    </div>
                                    <div class="col-sm-3 text-sm-right">
                                    <span class="reviews">
                                        <span>5</span>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i> 
                                    </span>
<!--                                        <a href="{ { route('add.to.cart', $product->products_id) }}" class="select">Add to Cart</a>-->
                                        {{-- <a >Select</a> --}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </section>


   

    <div class="modal fade cart-second-modal" id="CartSecondModal" tabindex="-1" role="dialog"
         aria-labelledby="CartSecondModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>You Might Also Know</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card-box">
                                <img src="./images/listing-image.jpg">
                                <div class="box-padd">
                                    <h2>Edamame</h2>
                                    <h3 class="price">$9.54</h3>
                                    <a class="add-to-order-btn" href="#">yes,add to order</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card-box">
                                <img src="./images/listing-image.jpg">
                                <div class="box-padd">
                                    <h2>Edamame</h2>
                                    <h3 class="price">$9.54</h3>
                                    <a class="add-to-order-btn" href="#">yes,add to order</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card-box">
                                <img src="./images/listing-image.jpg">
                                <div class="box-padd">
                                    <h2>Edamame</h2>
                                    <h3 class="price">$9.54</h3>
                                    <a class="add-to-order-btn" href="#">yes,add to order</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-5">
                        <div class="col-12 text-center">
                            <a class="no-go-to-checkout" href="#">no,go to checkout</a>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade checkout-modal" id="SpecialInstructionModal" tabindex="-1" role="dialog"
         aria-labelledby="SpecialInstructionModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
<!--                            <img src="./images/listing-image.jpg" class="checkout-image">-->
                            <img id="productImage" src="" class="checkout-image">
                            <h1 id="productTitle"></h1>
                            <p id="productDesc"></p>
                        </div>
                        <div class="col-sm-6 py-5">
                            <h2>Special Instructions</h2>
                            <textarea placeholder="Add any special instructions for this item here!" name=""
                                      id="ProductInstruction"></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-sm-6"> 
                          <div class="">
                                <i class="fa fa-minus"></i>
                                <span> <input type="number" id="productQuantity" min="1" value="" class="form-control quantity update-cart" placeholder="1" style="display: inline-block;width: 14%;"/></span>
                                <i class="fa fa-plus"></i>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="add-to-order">
                                <a id="addToCartButton" href="#" class="add-order">Add to Order</a>
                                   
                                <span class="amount" id="productPrice"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('footer')
    @parent
    <script>



        // $('#addToCartButton').click(function (){
        //    var numOfProduct = $('#productQuantity').val();
        //     var ProductPrice = $('#productPrice').val();
        //     var instructions = $('#ProductInstruction').val();
        //     console.log(numOfProduct);
        //     var url = '{{ route("add.to.carts") }}';

        //     $.ajax({
        //         url : url,
        //         type: 'POST',
        //         data: {
        //             _token: '{{ csrf_token() }}',
        //             price:ProductPrice,
        //             quantity:numOfProduct,
        //             instructions:instructions,
        //         },
        //         dataType: "json",
        //         cache:false
        //     }).done(function(data){
        //         if(data.data ==true){
        //             console.log(data.product_data);

        //         }
        //         if(!data.error){

        //             //    location.reload();
        //         }
        //     }).fail(function(data){

        //     });


        // });

        $("#searchbox_vendor").change(function(){
          var vendorP =  $('#searchbox_vendor').find(":selected").val();

            var url = '{{ route("vendorLists", ":id") }}';
            url = url.replace(':id', vendorP);

            window.location.href = url;



            var urlstaff = '<?php echo URL::to('/AdminDashboardController/vendorList'); ?>';
           if(vendorP != 'all'){

               var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({

                type: "POST",
                url: urlstaff,
                serverSide: true,
                data: {
                    _token: CSRF_TOKEN,
                    id:vendorP,
                },
                dataType: "json",
                cache:false,
                success : function(data){
                    console.log(data.data);
                    if (data.data == "true"){
                        alert_float('success','Default Staff Updated Successfully');
                        $('#default_staff_change option[value=default_staff_id]').attr('selected','selected');
                    } else {

                        alert_float('danger','Something went wrong.');
                    }

                }
            })
           }

        });

        $( document ).ready(function() {

            $('.show_product_popUp').click(function (){
         //       console.log($(this));

                var adminurl = '{{ url("/") }}';

                var url = '{{ route("productData") }}';

                var product_id = $(this).attr('data-product-id');


                $.ajax({
                    url : url,
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id:product_id},
                    dataType: "json",
                    cache:false
                }).done(function(data){
                    if(data.data ==true){
                        console.log(data.product_data);
                        var assetUrl= '{{asset('')}}'+data.product_data.image_url;


                        $('#productTitle').text(data.product_data.title);
                        $('#productDesc').text(data.product_data.description);
                        $('#productImage').attr('src', assetUrl);
                        $('#productPrice').text('$'+data.product_data.price);
                        $('#addToCartButton').attr('href',adminurl+'/add-to-cart/'+product_id)
                        $('#SpecialInstructionModal').modal('show');

                    }
                    if(!data.error){

                    //    location.reload();
                    }
                }).fail(function(data){

                });

            });
        });


        /*data-toggle="modal" data-target="#SpecialInstructionModal"*/


    </script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
