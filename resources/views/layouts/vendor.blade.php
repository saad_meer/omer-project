<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @section('head')
        @include('vendor.includes.head')
        <!--  BEGIN CUSTOM STYLE FILE  -->
            <link rel="stylesheet" type="text/css" href="{{asset('vendor-dashboard/assets/css/elements/alert.css')}}">
            <style>
                .btn-light { border-color: transparent; }
            </style>
            <!--  END CUSTOM STYLE FILE  -->
    @show
</head>
<body class="sidebar-noneoverflow">
    @include('vendor.includes.header')

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="cs-overlay"></div>
        <div class="search-overlay"></div>

        @include('vendor.includes.sidebar')

    <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="page-header">
                    <div class="page-title">
                        <h3>@yield('page-title')</h3>
                    </div>

<!--                    <div class="row error-display-bar">

                        @if ($errors->any())
                            <div class="alert alert-outline-danger">
                                <ul class="">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>-->
                </div>

                <div class="row layout-spacing layout-top-spacing">
                    @yield('content')
                </div>
            </div>

            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">Copyright © 2022 <!--<a target="_blank" href="http://members-one.com.au/">Members-One</a>,--> All rights reserved.</p>
                </div>
                <div class="footer-section f-section-2">
                    <p class=""><!--Developed by  <strong><a href="mailto:khadim.nu@gmail.com">Khadim Raath</a></strong> with -->
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>
                </div>
            </div>
        </div>
    </div>

    @section('footer')
        @include('vendor.includes.footer')
    @show
</body>
</html>
