<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @section('head')
        @include('main.includes.head')
    @show
</head>
<body>

@include('main.includes.header')

@yield('content')
 
<div class="modal fade cart-modal" id="CartModal" tabindex="-1" role="dialog" aria-labelledby="CartModal"
aria-hidden="true">
<div class="modal-dialog" role="document">
   <div class="modal-content">
       <div class="modal-header">
           <h1>Cart</h1>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
       </div>
       <div class="modal-body">
           <table class="table">
               <thead>
               <tr>
                   <th scope="col">Name</th>
                   <th scope="col">Quantity</th>
                   <th scope="col">Price</th>
               </tr>
               </thead>
               <tbody>
                @php $total = 0 @endphp 
                @if(session('cart'))

                    @foreach(session('cart') as $id => $details)
                        @php $total += $details['price'] * $details['quantity'] @endphp
               <tr>
                    <td>{{ $details['name'] }}</td>
                   <td> <i class="fa fa-minus"></i>
                       <span>{{ $details['quantity'] }}</span>
                       <i class="fa fa-plus"></i>
                   </td>
                   <td>${{ $details['price'] }}</td>
               </tr> 
               @endforeach

                @endif
               <tr>
                   <td colspan="3"><a href="{{url('/checkout')}}"
                                      class="checkout-modal-btn">Checkout</a></td>
               </tr>
               </tbody>
           </table>


       </div>
       <div class="modal-footer">

       </div>
   </div>
</div>
</div>
<!--  BEGIN MAIN CONTAINER  -->
<footer class="marketing-footer ">
    <div class="container">
        <div class="row py-5">
            <div class="col-sm-4 col-12">
                <div class="marketing-footer__product-description">
                    Order business catering from<br>
                    83,187 restaurants nationwide
                </div>
                <div class="social-icons">
                    <a class="marketing-footer__social-link" aria-label="linkedin" rel="noopener noreferrer"
                       href="https://www.linkedin.com/company/INSTAKTER" auto-tracked="true">
                        <img width="25" height="28" alt="LinkedIn" class=" lazyloaded" src="{{asset('images/img/linkin.svg')}}">
                    </a>
                    <a class="marketing-footer__social-link" aria-label="facebook" rel="noopener noreferrer"
                       href="https://www.facebook.com/pages/INSTAKTER/157973357563522" auto-tracked="true">
                        <img width="25" height="28" alt="Facebook" class=" lazyloaded" src="{{asset('images/img/fb.svg')}}">
                    </a>
                    <a class="marketing-footer__social-link" aria-label="twitter" rel="noopener noreferrer"
                       href="https://twitter.com/INSTAKTER" auto-tracked="true">
                        <img width="25" height="28" alt="Twitter" class=" lazyloaded" src="{{asset('images/img/twitter.svg')}}">
                    </a>
                    <a class="marketing-footer__social-link" aria-label="instagram" rel="noopener noreferrer"
                       href="https://www.instagram.com/INSTAKTER/" auto-tracked="true">
                        <img width="25" height="28" alt="Instagram" class=" lazyloaded"
                             src="{{asset('images/img/instagram.svg')}}">
                    </a>
                </div>
            </div>
            <div class="col-sm-3 pt-4 pt-sm-0 col-4">
                <div class="marketing-footer__links-block">
                    <h3 class="marketing-footer__links-title">Company</h3>
                    <ul>
                        <li><a class="marketing-footer__link" href="https://www.instakter.com/company/about-us/">About
                                Us</a></li>
                        <li><a class="marketing-footer__link"
                               href="https://www.instakter.com/company/careers/">Careers</a></li>
                        <li><a class="marketing-footer__link" href="/lunchrush">Blog</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3 col-4 pt-4 pt-sm-0">
                <div class="marketing-footer__links-block">
                    <h3 class="marketing-footer__links-title">Info</h3>
                    <ul>
                        <li>
                            <a class="marketing-footer__link" data-remote="true"
                               href="https://www.instakter.com/company/contact-us/">Contact Us</a>
                        </li>
                        <li><a class="marketing-footer__link" href="https://www.instakter.com/company/faqs/">FAQ</a>
                        </li>
                        <li><a class="marketing-footer__link" href="https://reviews.instakter.com">Best Caterer
                                Reviews</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 pt-4 pt-sm-0 col-4">
                <div class="marketing-footer__links-block">
                    <h3 class="marketing-footer__links-title">Get Started</h3>
                    <ul>
                        <li><a class="marketing-footer__link" data-role="scroll-to" data-anchor="#main-body-section"
                               href="javascript:void(0)">Start an Order</a></li>
                        <li><a class="marketing-footer__link"
                               href="https://www.instakter.com/company/grow-catering-business/">Become a Caterer</a>
                        </li>
                        <li><a class="marketing-footer__link"
                               href="https://www.instakter.com/company/enterprise/">Enterprise</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="row">
        <div class="col-sm-12">
            <hr>
           </div>
        </div>
        <div class="row py-5">
            <div class="col-sm-4">
            <div class="marketing-footer__links-block">
                    <h3 class="marketing-footer__links-title">States</h3>
                  
                </div>
            </div>
            <div class="col-sm-2 col-6 pt-4 pt-sm-0">
                <div class="marketing-footer__links-block">
              
                    <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-2 col-6  pt-4 pt-sm-0">
                <div class="marketing-footer__links-block">
                  
                     <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 col-6 pt-sm-0">
                <div class="marketing-footer__links-block">
                   
                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 col-6 pt-sm-0">
                <div class="marketing-footer__links-block">
                   
                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Alabama</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="row">
        <div class="col-sm-12">
            <hr>
           </div>
        </div>
        <div class="row py-5">
            <div class="col-sm-4">
            <div class="marketing-footer__links-block">
                    <h3 class="marketing-footer__links-title">Top Brands</h3>
                  
                </div>
            </div>
            <div class="col-sm-2 col-6  pt-4 pt-sm-0">
                <div class="marketing-footer__links-block">
              
                    <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chili's Grill & Bar</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Moe's Southwest Grill</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Potbelly Sandwich Shop</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-2 col-6  pt-4 pt-sm-0">
                <div class="marketing-footer__links-block">
                 
                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chili's Grill & Bar</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Moe's Southwest Grill</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Potbelly Sandwich Shop</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 col-6  pt-sm-0">
                <div class="marketing-footer__links-block">
              
                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chili's Grill & Bar</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Moe's Southwest Grill</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Potbelly Sandwich Shop</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 col-6 pt-sm-0">
                <div class="marketing-footer__links-block">

                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chili's Grill & Bar</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Moe's Southwest Grill</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Potbelly Sandwich Shop</a></li>
                    </ul>
                </div>
            </div>

        </div>
       
        <div class="row">
           <div class="col-sm-12">
               <hr>
           </div>
        </div>
        <div class="row py-5">
            <div class="col-sm-4">
            <div class="marketing-footer__links-block">
                    <h3 class="marketing-footer__links-title">Food Types</h3>
                  
                </div>
            </div>
            <div class="col-sm-2 col-6 pt-4 pt-sm-0">
                <div class="marketing-footer__links-block">
              
                    <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Taco</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Breakfast</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Italian</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chinese</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Boxed Lunch</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-2 col-6  pt-4 pt-sm-0">
                <div class="marketing-footer__links-block">
                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Taco</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Breakfast</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Italian</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chinese</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Boxed Lunch</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 col-6  pt-sm-0">
                <div class="marketing-footer__links-block">
                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Taco</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Breakfast</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Italian</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chinese</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Boxed Lunch</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 col-6   pt-sm-0">
                <div class="marketing-footer__links-block">
                <ul>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Taco</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Breakfast</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Italian</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Chinese</a></li>
                        <li><a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Boxed Lunch</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="row">
           <div class="col-sm-12">
               <hr>
           </div>
        </div>
        <div class="row py-2">
           
            <div class="col-sm-12 pt-4 pt-sm-0 text-center copyrights">
                    <span>© 2022 ezCater, Inc</span>                  
                        <a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Privacy Policy</a>
                        <a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Terms</a>
                        <a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">California Notice at Collection</a>
                        <a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Accessibility</a>
                        <a class="marketing-footer__link" href="https://www.ezcater.com/company/about-us/">Do Not Sell My Info</a>

            </div>

            
        </div>
    </div>
</footer>
@section('footer')
    @include('main.includes.footer')
@show
</body>
</html>
