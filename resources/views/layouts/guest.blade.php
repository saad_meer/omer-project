<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name', 'Membersone') }}</title>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/auth/plugins.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/auth/authentication/form-1.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/auth/forms/theme-checkbox-radio.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/auth/forms/switches.css') }}">
        <!-- Styles -->
        @livewireStyles
        <!-- Scripts -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
{{--            <link rel="stylesheet" type="text/css" href="{{ asset('css/vendor/main.css') }}">--}}
        <link rel="stylesheet" type="text/css" href="{{ asset('css/auth/custom.css') }}">
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="form">
        <div class="font-sans text-gray-900 antialiased">
            {{ $slot }}
        </div>
        <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
        <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>

        <!-- END GLOBAL MANDATORY SCRIPTS -->
        <script src="{{ asset('js/auth/authentication/form-1.js') }}"></script>
    </body>
    </html>
