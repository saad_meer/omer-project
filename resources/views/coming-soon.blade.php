@extends('layouts.main')
@section('title', 'Category')
@section('head')
  @parent
  <link rel="stylesheet" type="text/css" href="{{ asset('admin-dashboard/plugins/table/datatable/datatables.css') }}">
@endsection
@section('content')


  <section class="banner">
    <div class="container">
      <div class="row pt-5">
        <div class="col-12">
          <h1>Food that works</h1>
          <h2>For team meetings</h2>
        </div>
      </div>
      <div class="row pt-5 banner-search">
        <div class="col-12 ">
          <p>Order business catering from 83,221 restaurants nationwide.</p>
          <div class="search-div">
            <i class="fa fa-map-marker"></i>            
            <input type="text" placeholder="Enter your delivery address" class="search-bar">
            <button type="submit" class="order-box__submit" aria-label="Search for caterers">
              Search
            </button>
          </div>

        </div>
      </div>
    </div>
  </section>

  <section class="learn-more">
    <img class="right-image" src="{{asset('images/img/right-image.png')}}" alt="right-image" />
    <img class="left-image" src="{{asset('images/img/left-image.png')}}" alt="left-image" />
    <div class="container">
      <div class="row py-5 my-sm-5">
        <div class="col-sm-4 home-learn-more-value-prop">
          <h2 class="home-learn-more-header">
            Flexible. For the modern workplace.
          </h2>
          <p class="text-base">
            Food solutions that scale to fit your needs and provide a benefit they'll love, without fixed
            costs or food waste
          </p>
          <a class="text-base font-bold section-copy--blue" href="/company/corporate/"
             data-click-tracking-name="flexible-for-modern-workplace">Learn more →</a>
        </div>
        <div class="col-sm-4 home-learn-more-value-prop">
          <h2 class="home-learn-more-header">
            Flexible. For the modern workplace.
          </h2>
          <p class="text-base">
            Food solutions that scale to fit your needs and provide a benefit they'll love, without fixed
            costs or food waste
          </p>
          <a class="text-base font-bold section-copy--blue" href="/company/corporate/"
             data-click-tracking-name="flexible-for-modern-workplace">Learn more →</a>
        </div>
        <div class="col-sm-4 home-learn-more-value-prop">
          <h2 class="home-learn-more-header">
            Flexible. For the modern workplace.
          </h2>
          <p class="text-base">
            Food solutions that scale to fit your needs and provide a benefit they'll love, without fixed
            costs or food waste
          </p>
          <a class="text-base font-bold section-copy--blue" href="/company/corporate/"
             data-click-tracking-name="flexible-for-modern-workplace">Learn more →</a>
        </div>
      </div>
    </div>
  </section>


  <section class="trusted-by">
    <div class="container">
      <div class="row py-5">
        <div class="col-12 text-center">
          <h2 class="uppercase">BUSINESS CATERING SOLUTIONS TRUSTED BY</h2>
        </div>
        <div class="col-12 text-center py-3">
          <img src="{{asset('images/img/logos.png')}}" alt="logos">
        </div>
      </div>
    </div>
  </section>


  <section class="green-section">
    <div class="container">
      <div class="row py-5">
        <div class="col-sm-6">
          <img src="{{asset('images/img/green-section-image.png')}}" alt="logos">
        </div>
        <div class="col-sm-6 text-center">
          <div class="boxed-lunches-text-container">
            <h2 class="boxed-lunches-heading section-header section-copy--white">
              Find boxed lunches anywhere in the US
            </h2>

            <p class="boxed-lunches-paragraph section-copy section-copy--paragraph section-copy--white">
              Grab and go options from your favorite local restaurants satisfy any size group, any budget,
              any palate.
            </p>

            <p class="section-cta section-copy--white">
              <strong>
                <a class="boxed-lunches-cta-link" data-click-tracking-name="find_boxed_lunches"
                   href="/delivery/boxed-lunch-catering">Find boxed lunches →</a>
              </strong>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="green-section support-company">
    <div class="container">
      <div class="row py-5">
       
        <div class="col-sm-6 text-center py-5">
          <div class="boxed-lunches-text-container">
            <h2 class="boxed-lunches-heading section-header section-copy--white"> Support all of your company’s food for work needs
            </h2>

            <p class="boxed-lunches-paragraph section-copy section-copy--paragraph section-copy--white">
              
          With ezCater Corporate Solutions, view food spending and simplify expense reports across your whole organization. Get one invoice across dozens of restaurants (no more maxed out credit cards!).
        
            </p>

            <p class="section-cta section-copy--white">
              <strong>
                <a class="boxed-lunches-cta-link" data-click-tracking-name="find_boxed_lunches"
                   href="/delivery/boxed-lunch-catering">Learn More →</a>
              </strong>
            </p>
          </div>
        </div>
        <div class="col-sm-6 py-5">
          <img src="{{asset('images/img/screen.png')}}" alt="logos">
        </div>
      </div>
    </div>
  </section>
  <section class="green-section setup-employee">
    <div class="container">
      <div class="row py-5">
        <div class="col-sm-6">
          
          <img src="{{asset('images/img/mobile-screen.png')}}" alt="logos">
        </div>
        <div class="col-sm-6 text-center">
          <div class="boxed-lunches-text-container">
            <h2 class="boxed-lunches-heading section-header">
            Set up daily employee meals
            </h2>

            <p class="boxed-lunches-paragraph section-copy section-copy--paragraph">
            Keep your people healthy and
happy by offering individually
packaged meals from a variety of
local restaurants, delivered
contactlessly to workplaces across
the U.S.
            </p>

            <p class="section-cta">
              <strong>
                <a class="boxed-lunches-cta-link" data-click-tracking-name="find_boxed_lunches"
                   href="/delivery/boxed-lunch-catering">Learn More →</a>
              </strong>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="weve-got-you-covered">
    <div class="weve-got-you-covered-content container">
      <div class="row">
        <div class="col-12">
          <h2 class="weve-got-you-covered-heading">
            Wherever your meeting is, <br>
            we've got you covered
          </h2>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3 col-6">
          <figure class="weve-got-you-covered-stat-figure">
            <img width="60" height="60" alt="Storefront icon" class="weve-got-you-covered-icon"
                 src="{{asset('images/img/icon-1.svg')}}">
          </figure>
          <figcaption>
            <p class="weve-got-you-covered-stat-paragraph">
              83,214
            </p>
            <p class="weve-got-you-covered-stat-label">
              Restaurants
            </p>
          </figcaption>
        </div>

        <div class="col-sm-3 col-6">
          <figure class="weve-got-you-covered-stat-figure">
            <img width="60" height="60" alt="Storefront icon" class="weve-got-you-covered-icon"
                 src="{{asset('images/img/icon-2.svg')}}">
          </figure>
          <figcaption>
            <p class="weve-got-you-covered-stat-paragraph">
              10,698,308
            </p>
            <p class="weve-got-you-covered-stat-label">
              Catering Ratings <br class="lg:hidden">&amp; Reviews
            </p>
          </figcaption>
        </div>

        <div class="col-sm-3 col-6">
          <figure class="weve-got-you-covered-stat-figure">
            <img width="60" height="60" alt="Storefront icon" class="weve-got-you-covered-icon"
                 src="{{asset('images/img/icon-3.svg')}}">
          </figure>
          <figcaption>
            <p class="weve-got-you-covered-stat-paragraph">
              22,743
            </p>
            <p class="weve-got-you-covered-stat-label">
              Cities Served
            </p>
          </figcaption>
        </div>

        <div class="col-sm-3 col-6">
          <figure class="weve-got-you-covered-stat-figure">
            <img width="60" height="60" alt="Storefront icon" class="weve-got-you-covered-icon"
                 src="{{asset('images/img/icon-4.svg')}}">
          </figure>
          <figcaption>
            <p class="weve-got-you-covered-stat-paragraph">
              191,188,489
            </p>
            <p class="weve-got-you-covered-stat-label">
              People Served
            </p>
          </figcaption>
        </div>
      </div>
    </div>
    </div>

    </div>
    </div>
  </section>


  <section class="grow-and-manage">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 py-5">
          <h2 class="grow-and-manage-heading section-header">
            Grow and manage your catering business
          </h2>

          <div class="grow-and-manage-paragraph section-copy section-copy--paragraph">
            <p></p>
            <p>Get more catering orders through
              <br>our online marketplace, where
              <br>millions of business people order
              <br>food for meetings and company
              <br>events from over 83,214
              <br>restaurants.
            </p>
            <p></p>
          </div>

          <p class="grow-and-manage-cta-container section-cta section-copy--blue">
            <a data-click-tracking-name="grow_and_manage_cta"
               aria-label="learn more about INSTAKTER for caterers"
               href="/company/grow-catering-business">Learn more →</a>
          </p>
        </div>
        <div class="col-sm-4">
          <img width="765" height="600" alt="Collage of catering business owners"
               class="grow-and-manage-image-tablet-up lazyloaded" src="{{asset('images/img/img.png')}}">
        </div>
      </div>
    </div>
  </section>

@endsection

@section('footer')
  @parent
  <!-- BEGIN PAGE LEVEL SCRIPTS -->

  <!-- END PAGE LEVEL SCRIPTS -->
@endsection
