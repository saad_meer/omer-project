<header>
    <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{url('/')}}">
           INSTAKER
        </a>                   
            <a href="#" data-toggle="modal" data-target="#CartModal" class="btn btn-info d-lg-none">

                <i class="fa fa-shopping-cart " aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
            </a>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/INSTAKTER')}}">Brands</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">Sign in</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('register')}}">Sign up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">1-800-488-1803</a>
                </li>
                <li class="nav-item d-lg-block d-none">
                   
                <a href="#" data-toggle="modal" data-target="#CartModal" class="btn btn-info">

                    <i class="fa fa-shopping-cart " aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                </a>
                </li>
            </ul>
            {{-- <div class="dropdown">

                <a href="#" data-toggle="modal" data-target="#CartModal" class="btn btn-info">

                    <i class="fa fa-shopping-cart " aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>

                    </b>

                    <div class="dropdown-menu">

                        <div class="row total-header-section">

                            <div class="col-lg-6 col-sm-6 col-6">

                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>

                            </div>

                            @php $total = 0 @endphp

                            @foreach((array) session('cart') as $id => $details)

                                @php $total += $details['price'] * $details['quantity'] @endphp

                            @endforeach

                            <div class="col-lg-6 col-sm-6 col-6 total-section text-right">

                                <p>Total: <span class="text-info">$ {{ $total }}</span></p>

                            </div>

                        </div>

                        @if(session('cart'))

                            @foreach(session('cart') as $id => $details)

                                <div class="row cart-detail">

                                    <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">

                                        <img src="{{ $details['image'] }}" />

                                    </div>

                                    <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">

                                        <p>{{ $details['name'] }}</p>

                                        <span class="price text-info"> ${{ $details['price'] }}</span> <span class="count"> Quantity:{{ $details['quantity'] }}</span>

                                    </div>

                                </div>

                            @endforeach

                        @endif

                        <div class="row">

                            <div class="col-lg-12 col-sm-12 col-12 text-center checkout">

                                <a href="{{ route('cart') }}" class="btn btn-primary btn-block">View all</a>

                            </div>

                        </div>

                    </div>

            </div> --}}

        </div>
    </nav>
</header>