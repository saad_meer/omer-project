<x-guest-layout>
    <div class="form-container">
        <div class="form-image"  style="right: unset ;">
            <div class="l-image p-2" style="background-image: url({{ asset('images/bg-buiding.jpg') }});background-size: cover; color: white;">

                <div  style="height:100vh;padding-top: 80vh;">
{{--                    <h1 class="display-4 " style="color: white;">{{ config('app.name', 'Membersone') }}</h1>--}}
                    <h1 class="display-4 " style="color: white;">Lorem Ipsem</h1>

                    <div  class="d-flex pl-1">
                        <svg  xmlns="http://www.w3.org/2000/svg"   width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin svg-marginTop p-0 m-0"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>
                        <p style="color: white;" class="pl-2">
                            Lorem Ipsem
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-form" style="margin-left: 50%; " >
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">
                        <x-slot name="logo">
                            <img src="{{asset('images/company_transparrent_background.png')}}" class="page-logo" alt="{{ config('app.name', 'MembersOne') }}">
                        </x-slot>
                        <h1 class="text-center">Main Logo imgg</h1>
                        <br>
<!--                        <img src="{{asset('images/company_transparrent_background.png')}}" class="page-logo" alt="{{ config('app.name', 'MembersOne') }}">-->
                        <p >This platform is interacting with {{ config('app.name', 'MembersOne') }}.  </p>
                        <br>
                        <x-jet-validation-errors class="mb-4" />

                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-green-600">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form method="POST" action="{{ route('login') }}" class="text-left">
                                @csrf
                            <div class="form">

                                <div id="useremail-field" class="field-wrapper input " >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user " ><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                    <input id="useremail" type="email" name="email" :value="old('email')" class="form-control" placeholder="User Email Id" style="padding-top:10px;">

                                </div>
                                <div id="password-field" class="field-wrapper input mb-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock" ><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                    <input id="password" type="password" name="password" :value="old('password')" class="form-control" placeholder="User Password" style="padding-top:10px;">

                                </div>

                                <div class="">
                                    <label for="remember_me" class="flex items-center">
                                        <x-jet-checkbox id="remember_me" name="remember" />
                                        <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                                    </label>
                                </div>
                                <br>


                                <div class="d-sm-flex justify-content-center">

                                    <div class="field-wrapper">
                                        <a href="{{ route('password.request') }}" class="forgot-pass-link" >Forgot Password?</a>
                                    </div>
                                </div>
                                <div class="d-sm-flex  mt-3">
                                    <div class="field-wrapper w-100">
                                        <button type="submit" class="btn btn-primary btn-block"  value="">Log In</button>
                                    </div>
                                </div>
                                <div class="d-sm-flex justify-content-center mt-4">
                                    <div class="field-wrapper">
                                        <p>
                                            Don’t have an account please sign up
                                        </p>
                                    </div>
                                </div>
                                <div class="d-sm-flex ">
                                    <div class="field-wrapper w-100">
                                        @if (Route::has('register'))
                                            <a href="{{ route('register') }}" class="btn btn-primary btn-block" >Register</a>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </x-guest-layout>
