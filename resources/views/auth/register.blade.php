<x-guest-layout>
    <div class="form-container">
        <div class="form-image"  style="right: unset ;">
            <div class="l-image p-2" style="background-image: url({{ asset('images/bg-buiding.jpg') }});background-size: cover; color: white;">

                <div  style="height:100vh;padding-top: 80vh;">
                    <h1 class="display-4 " style="color: white;">{{ config('app.name', 'Membersone') }}</h1>

                    <div  class="d-flex pl-1">
                        <svg  xmlns="http://www.w3.org/2000/svg"   width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin svg-marginTop p-0 m-0"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>
                        <p style="color: white;" class="pl-2">
                            Lahore, Pakistan
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-form" style="margin-left: 50%;" >
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">
                        <x-slot name="logo">
                            <img src="{{asset('images/images/img/logo.PNG')}}" class="page-logo" alt="{{ config('app.name', 'INSTAKTER') }}">
                        </x-slot>
                        <img src="{{asset('images/img/logo.PNG')}}" class="page-logo" alt="{{ config('app.name', 'INSTAKTER') }}">
<!--                        <h1 class="">Get started to schedule orders</h1>-->
                        <br>
                        <p class="signup-link">Already have an account? <a href="{{ route('login') }}">Log in</a></p>
                        <br>

                        <x-jet-validation-errors class="mb-4" />

                        <form method="POST" action="{{ route('registervendor') }}" class="text-left">
                            @csrf
                            <input type="hidden" name="_role" value="vendor">
                            <div class="form">

                                <div class="mt-4 field-wrapper input">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                                    <x-jet-input id="name" class="block mt-1 w-full" placeholder="Name" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                                </div>

                                <div id="email-field" class="mt-4 field-wrapper input">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-at-sign"><circle cx="12" cy="12" r="4"></circle><path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94"></path></svg>
                                    <x-jet-input id="email" class="block mt-1 w-full" placeholder="Email" type="email" name="email" :value="old('email')" required />
                                </div>

                                <div class="mt-4 field-wrapper input">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                    <x-jet-input id="password" class="block mt-1 w-full" placeholder="Password" type="password" name="password" required autocomplete="new-password" />
                                </div>

                                <div class="mt-4 field-wrapper input">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg>
                                    <x-jet-input id="password_confirmation" placeholder="Confirm Password" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
                                </div>

                                @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                                    <div class="mt-4 field-wrapper terms_condition">
                                        <x-jet-label for="terms" class="new-control new-checkbox checkbox-outline-primary">
                                            <div class="flex items-center">
                                                <x-jet-checkbox name="terms" id="terms" class="new-control-input"/>
                                                <div class="ml-2 new-control-indicator">
                                                    {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                                            'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                                            'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                                    ]) !!}
                                                </div>
                                            </div>
                                        </x-jet-label>
                                    </div>
                                @endif

                                <div class="flex items-center justify-end mt-4">

                                    <div class="d-sm-flex justify-content-between">
                                        <div class="field-wrapper toggle-pass">
                                            <p class="d-inline-block">Show Password</p>
                                            <label class="switch s-primary">
                                                <input type="checkbox" id="toggle-password" class="d-none">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="field-wrapper">
                                            <x-jet-button class="ml-4 btn btn-primary">
                                                {{ __('Get Started!') }}
                                            </x-jet-button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
