# members-one

Members one.

## System Requirements

- Php 7.4
- Minimum of Mysql 5.6
- composer
- apache

## Known issues
- Not any known issue

### Deployment steps:

- Goto to Project directory``cd members-one``
- create database **members-one**
- Adjust DB access credentials in .env
- Run ``composer install`` command
- ``php artisan lumki:setup`` and don't run migrations & Don't run seed.
- ``php artisan migrate``
- ``php artisan db:seed``
- Install laravel Cashier without migration.
    ``composer require laravel/cashier``
- ``sudo chmod -R 777 storage/``


## Built With

* Laravel 8

## Versioning

### v1.0.0 (16-08-2021)

- A complete working project
## Authors

* **M Awais** - [github profile](https://github.com/AwaiX728)

See also the list of [contributors](https://github.com/kraath/members-one/graphs/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
